//
//  GameScene.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GorillaDrummerDefines.h"
#import "MMWavesSpriteNode.h"

@interface GorillaDrummerScene : SKScene

@property (nonatomic, strong, readonly) MMWavesSpriteNode* wavesSpriteNode;

- (void)showGorillaDrummerExpressionAnimation:(GorillaDrummerExpression)expression;
- (void)showGorillaBeatAnimationForType:(GorillaDrummerBeatType)beatType;
- (void)showConfettiAnimation;

- (void)showTextAnimation:(NSString *)text awhile:(BOOL)awhile;
- (void)hideTextAnimation;

- (void)showCoinsTextAnimation:(NSString *)text
                 numberOfCoins:(NSInteger)numberOfCoins
                        awhile:(BOOL)awhile;
- (void)hideCoinsTextAnimaiton;

- (void)updateSceneStyle:(GorillaDrummerSceneStyle)style;

@end

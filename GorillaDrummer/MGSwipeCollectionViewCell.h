//
//  MGSwipeCollectionViewCell.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 2/20/18.
//  Copyright © 2018 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeButton.h"

typedef NS_ENUM(NSInteger, MGSwipeTransition) {
    MGSwipeTransitionBorder = 0,
    MGSwipeTransitionStatic,
    MGSwipeTransitionDrag,
    MGSwipeTransitionClipCenter,
    MGSwipeTransitionRotate3D
};

#define MGSwipeTransition3D MGSwipeTransitionRotate3D
#define MGSwipeStateSwippingLeftToRight MGSwipeStateSwipingLeftToRight
#define MGSwipeStateSwippingRightToLeft MGSwipeStateSwipingRightToLeft

typedef NS_ENUM(NSInteger, MGSwipeDirection) {
    MGSwipeDirectionLeftToRight = 0,
    MGSwipeDirectionRightToLeft
};

typedef NS_ENUM(NSInteger, MGSwipeState) {
    MGSwipeStateNone = 0,
    MGSwipeStateSwipingLeftToRight,
    MGSwipeStateSwipingRightToLeft,
    MGSwipeStateExpandingLeftToRight,
    MGSwipeStateExpandingRightToLeft,
};

typedef NS_ENUM(NSInteger, MGSwipeExpansionLayout) {
    MGSwipeExpansionLayoutBorder = 0,
    MGSwipeExpansionLayoutCenter,
    MGSwipeExpansionLayoutNone
};

typedef NS_ENUM(NSInteger, MGSwipeEasingFunction) {
    MGSwipeEasingFunctionLinear = 0,
    MGSwipeEasingFunctionQuadIn,
    MGSwipeEasingFunctionQuadOut,
    MGSwipeEasingFunctionQuadInOut,
    MGSwipeEasingFunctionCubicIn,
    MGSwipeEasingFunctionCubicOut,
    MGSwipeEasingFunctionCubicInOut,
    MGSwipeEasingFunctionBounceIn,
    MGSwipeEasingFunctionBounceOut,
    MGSwipeEasingFunctionBounceInOut
};

@interface MGSwipeAnimation : NSObject

@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, assign) MGSwipeEasingFunction easingFunction;

-(CGFloat) value:(CGFloat) elapsed duration:(CGFloat) duration from:(CGFloat) from to:(CGFloat) to;

@end

@interface MGSwipeSettings: NSObject

@property (nonatomic, assign) MGSwipeTransition transition;
@property (nonatomic, assign) CGFloat threshold;
@property (nonatomic, assign) CGFloat offset;
@property (nonatomic, assign) CGFloat topMargin;
@property (nonatomic, assign) CGFloat bottomMargin;
@property (nonatomic, assign) CGFloat buttonsDistance;
@property (nonatomic, assign) BOOL expandLastButtonBySafeAreaInsets;

@property (nonatomic, strong, nonnull) MGSwipeAnimation * showAnimation;
@property (nonatomic, strong, nonnull) MGSwipeAnimation * hideAnimation;
@property (nonatomic, strong, nonnull) MGSwipeAnimation * stretchAnimation;

@property (nonatomic, assign) CGFloat animationDuration DEPRECATED_ATTRIBUTE;

@property (nonatomic, assign) BOOL keepButtonsSwiped;
@property (nonatomic, assign) BOOL onlySwipeButtons;
@property (nonatomic, assign) BOOL enableSwipeBounces;
@property (nonatomic, assign) CGFloat swipeBounceRate;
@property (nonatomic) BOOL allowsButtonsWithDifferentWidth;

@end

@interface MGSwipeExpansionSettings: NSObject

@property (nonatomic, assign) NSInteger buttonIndex;
@property (nonatomic, assign) BOOL fillOnTrigger;
@property (nonatomic, assign) CGFloat threshold;
@property (nonatomic, strong, nullable) UIColor * expansionColor;
@property (nonatomic, assign) MGSwipeExpansionLayout expansionLayout;
@property (nonatomic, strong, nonnull) MGSwipeAnimation * triggerAnimation;

@property (nonatomic, assign) CGFloat animationDuration;

@end


@class MGSwipeCollectionViewCell;

@protocol MMSwipeCollectionViewCellDelegate <NSObject>

@optional

-(BOOL) swipeCollectionViewCell:(nonnull MGSwipeCollectionViewCell*) cell canSwipe:(MGSwipeDirection) direction fromPoint:(CGPoint) point;
-(BOOL) swipeCollectionViewCell:(nonnull MGSwipeCollectionViewCell*) cell canSwipe:(MGSwipeDirection) direction DEPRECATED_ATTRIBUTE; //backwards compatibility

-(void) swipeCollectionViewCell:(nonnull MGSwipeCollectionViewCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive;
-(BOOL) swipeCollectionCell:(nonnull MGSwipeCollectionViewCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion;
-(nullable NSArray<UIView*>*) swipeCollectionViewCell:(nonnull MGSwipeCollectionViewCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(nonnull MGSwipeSettings*) swipeSettings expansionSettings:(nonnull MGSwipeExpansionSettings*) expansionSettings;
-(BOOL) swipeCollectionViewCell:(nonnull MGSwipeCollectionViewCell *)cell shouldHideSwipeOnTap:(CGPoint) point;
-(void) swipeCollectionViewWillBeginSwiping:(nonnull MGSwipeCollectionViewCell *) cell;
-(void) swipeCollectionViewCellWillEndSwiping:(nonnull MGSwipeCollectionViewCell *) cell;

@end

@interface MGSwipeCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak, nullable) id<MMSwipeCollectionViewCellDelegate> delegate;

@property (nonatomic, strong, readonly, nonnull) UIView * swipeContentView;

@property (nonatomic, copy, nonnull) NSArray<UIView*> * leftButtons;
@property (nonatomic, copy, nonnull) NSArray<UIView*> * rightButtons;
@property (nonatomic, strong, nonnull) MGSwipeSettings * leftSwipeSettings;
@property (nonatomic, strong, nonnull) MGSwipeSettings * rightSwipeSettings;

@property (nonatomic, strong, nonnull) MGSwipeExpansionSettings * leftExpansion;
@property (nonatomic, strong, nonnull) MGSwipeExpansionSettings * rightExpansion;

@property (nonatomic, readonly) MGSwipeState swipeState;
@property (nonatomic, readonly) BOOL isSwipeGestureActive;

@property (nonatomic) BOOL allowsMultipleSwipe;
@property (nonatomic) BOOL allowsButtonsWithDifferentWidth DEPRECATED_MSG_ATTRIBUTE("Use MGSwipeSettings.allowsButtonsWithDifferentWidth instead");
@property (nonatomic) BOOL allowsSwipeWhenTappingButtons;
@property (nonatomic) BOOL allowsOppositeSwipe;
@property (nonatomic) BOOL preservesSelectionStatus;
@property (nonatomic) BOOL touchOnDismissSwipe;

@property (nonatomic, strong, nullable) UIColor * swipeBackgroundColor;
@property (nonatomic, assign) CGFloat swipeOffset;

-(void) hideSwipeAnimated: (BOOL) animated;
-(void) hideSwipeAnimated: (BOOL) animated completion:(nullable void(^)(BOOL finished)) completion;
-(void) showSwipe: (MGSwipeDirection) direction animated: (BOOL) animated;
-(void) showSwipe: (MGSwipeDirection) direction animated: (BOOL) animated completion:(nullable void(^)(BOOL finished)) completion;
-(void) setSwipeOffset:(CGFloat)offset animated: (BOOL) animated completion:(nullable void(^)(BOOL finished)) completion;
-(void) setSwipeOffset:(CGFloat)offset animation: (nullable MGSwipeAnimation *) animation completion:(nullable void(^)(BOOL finished)) completion;
-(void) expandSwipe: (MGSwipeDirection) direction animated: (BOOL) animated;

-(void) refreshContentView;
-(void) refreshButtons: (BOOL) usingDelegate;

@end


//
//  MMCity.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/28/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMCity : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@property (nonatomic, strong, readonly) NSString* countryName;
@property (nonatomic, strong, readonly) NSString* cityName;

@end

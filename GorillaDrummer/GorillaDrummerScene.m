//
//  GameScene.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "GorillaDrummerScene.h"
#import "MMGorillaDrummerSprite.h"
#import "MMTextureManager.h"
#import "MMCoinsTextSprite.h"

@interface GorillaDrummerScene()

@property (nonatomic, strong) MMGorillaDrummerSprite* gorillaSpriteNode;
@property (nonatomic, strong) SKSpriteNode* bassSpriteNode;
@property (nonatomic, strong) SKSpriteNode* toms0SpriteNode;
@property (nonatomic, strong) SKSpriteNode* toms1SpriteNode;
@property (nonatomic, strong) SKSpriteNode* cymbolsSpriteNode;
@property (nonatomic, strong) SKSpriteNode* leftLightSpriteNode;
@property (nonatomic, strong) SKSpriteNode* rightLightSpriteNode;
@property (nonatomic, strong) SKSpriteNode* confettiSpriteNode;
@property (nonatomic, strong) SKSpriteNode* textGradientSpriteNode;
@property (nonatomic, strong) SKLabelNode* textLabelNode;
@property (nonatomic, strong) MMCoinsTextSprite* coinsTextContainerNode;

@property (nonatomic, assign) BOOL showingTextAnimation;

@end

@implementation GorillaDrummerScene

#pragma mark - SKView

- (void)didMoveToView:(SKView *)view {
  [super didMoveToView:view];
  
  [[MMTextureManager sharedInstance] preloadTextures];
  
  self.backgroundColor = [UIColor clearColor];
  
  [self _setupGorillaSpriteNode];
  [self _setupDrumSetNodes];
  [self _setupTextLabelNodes];
  [self _setupWavesSpriteNode];
}

#pragma mark - Setup

- (void)_setupGorillaSpriteNode {
  _gorillaSpriteNode = (MMGorillaDrummerSprite *)[self.scene childNodeWithName:@"gorilla"];
}

- (void)_setupDrumSetNodes {
  _bassSpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"bass"];
  _toms0SpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"toms_0"];
  _toms1SpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"toms_1"];
  _cymbolsSpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"cymbols"];
  _leftLightSpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"light_left"];
  _rightLightSpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"light_right"];
  _confettiSpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"confetti"];
  _confettiSpriteNode.hidden = YES;
  _textGradientSpriteNode = (SKSpriteNode *)[self.scene childNodeWithName:@"textGradientSpriteNode"];
  _textGradientSpriteNode.alpha = 0.0f;
}

- (void)_setupWavesSpriteNode {
  _wavesSpriteNode = [[MMWavesSpriteNode alloc] initWithSize:self.size];
  _wavesSpriteNode.anchorPoint = CGPointMake(0.5f, 0.5f);
  _wavesSpriteNode.position = CGPointMake(0.0f, 0.0f);
  [self insertChild:_wavesSpriteNode atIndex:0];
}

- (void)_setupTextLabelNodes {
  _textLabelNode = [[SKLabelNode alloc] initWithFontNamed:@"Helvetica-Regular"];
  _textLabelNode.fontSize = 50.0f;
  _textLabelNode.fontColor = [UIColor whiteColor];
  _textLabelNode.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
  _textLabelNode.position = CGPointMake(self.size.width/2.0f, self.size.height/2.0f);
  _textLabelNode.xScale = 0.3f;
  _textLabelNode.yScale = 0.3f;
  _textLabelNode.alpha = 0.0f;
  _textLabelNode.zPosition = 10;
  [self addChild:_textLabelNode];
  
  _coinsTextContainerNode = [[MMCoinsTextSprite alloc] init];
  _coinsTextContainerNode.zPosition = 11;
  _coinsTextContainerNode.alpha = 0.0f;
  [self addChild:_coinsTextContainerNode];
}

#pragma mark - Public Methods

- (void)updateSceneStyle:(GorillaDrummerSceneStyle)style {
  _leftLightSpriteNode.hidden = (style != GorillaDrummerSceneStylePurple);
  _rightLightSpriteNode.hidden = (style != GorillaDrummerSceneStylePurple);
}

- (void)showConfettiAnimation {
  if ([_confettiSpriteNode actionForKey:@"confettin_animation_action"]) {
    return;
  }
  
  _confettiSpriteNode.hidden = NO;
  NSArray<SKTexture *> *textures = [[MMTextureManager sharedInstance] confettiAnimation];
  SKAction* animationAction = [SKAction animateWithTextures:textures
                                               timePerFrame:(1.0f/30.0f)];
  __weak typeof(self) weakSelf = self;
  SKAction *completion = [SKAction runBlock:^{
    weakSelf.confettiSpriteNode.hidden = YES;
  }];
  SKAction *sequence = [SKAction sequence:@[animationAction, completion]];
  [_confettiSpriteNode runAction:sequence
                         withKey:@"confettin_animation_action"];
}

- (void)showGorillaDrummerExpressionAnimation:(GorillaDrummerExpression)expression {
  [_gorillaSpriteNode showExpressionAnimation:expression];
}

- (void)showGorillaBeatAnimationForType:(GorillaDrummerBeatType)beatType {
  [_gorillaSpriteNode showBeatAnimation:beatType];
  
  SKAction* moveToTheBottomAction = [SKAction moveBy:CGVectorMake(0.0f, -4.0f) duration:0.3f];
  SKAction* moveToTheTopAction = [SKAction moveBy:CGVectorMake(0.0f, 4.0f) duration:0.3f];
  
  switch (beatType) {
    case GorillaDrummerBeatType_0: {
      [self.toms0SpriteNode runAction:[SKAction sequence:@[
                                                           [SKAction waitForDuration:0.2f],
                                                           moveToTheBottomAction,
                                                           moveToTheTopAction
                                                          ]]];
    }
      break;
    case GorillaDrummerBeatType_1: {
      [self.toms1SpriteNode runAction:[SKAction sequence:@[
                                                           [SKAction waitForDuration:0.2f],
                                                           moveToTheBottomAction,
                                                           moveToTheTopAction
                                                          ]]];
    }
      break;
    case GorillaDrummerBeatType_2: {
      [self.toms1SpriteNode runAction:[SKAction sequence:@[
                                                           [SKAction waitForDuration:0.2f],
                                                           moveToTheBottomAction,
                                                           moveToTheTopAction
                                                          ]]];
      [self.bassSpriteNode runAction:[SKAction sequence:@[
                                                          [SKAction waitForDuration:0.2f],
                                                          [SKAction scaleTo:1.05f duration:0.2f],
                                                          [SKAction scaleTo:1.0f duration:0.2f]
                                                         ]]];
    }
      break;
    case GorillaDrummerBeatType_3: {
      [self.toms0SpriteNode runAction:[SKAction sequence:@[
                                                           [SKAction waitForDuration:0.2f],
                                                           moveToTheBottomAction,
                                                           moveToTheTopAction
                                                           ]]];
    }
      break;
      
    default: break;
  }
}

- (void)showTextAnimation:(NSString *)text awhile:(BOOL)awhile {
  if (_showingTextAnimation) {
    return;
  }
  _showingTextAnimation = YES;
  
  _textLabelNode.text = text;
  _textLabelNode.position = CGPointMake(0.0f, -50.0f);
  _textLabelNode.xScale = 0.3f;
  _textLabelNode.yScale = 0.3f;
  _textLabelNode.alpha = 1.0f;
  _textLabelNode.color = [UIColor redColor];
  
  [_textLabelNode removeAllActions];
  [_textLabelNode runAction:[SKAction group:@[
                                              [SKAction moveBy:CGVectorMake(0.0f, 75.0f) duration:0.3f],
                                              [SKAction fadeInWithDuration:0.3f],
                                              [SKAction scaleTo:1.0f duration:0.3f]
                                             ]]];
  
  [_textGradientSpriteNode runAction:[SKAction fadeInWithDuration:0.3f]];
  
  if (awhile) {
    [self performSelector:@selector(hideTextAnimation) withObject:nil afterDelay:2.0f];
  }
}

- (void)hideTextAnimation {
  if (_showingTextAnimation == NO) {
    return;
  }
  _showingTextAnimation = NO;
  
  [_textLabelNode removeAllActions];
  [NSObject cancelPreviousPerformRequestsWithTarget:self];
  [_textLabelNode runAction:[SKAction group:@[
                                              [SKAction moveBy:CGVectorMake(0.0f, -75.0f) duration:0.3f],
                                              [SKAction fadeOutWithDuration:0.3f],
                                              [SKAction scaleTo:0.3f duration:0.3f]
                                             ]]];
  
  [_textGradientSpriteNode runAction:[SKAction fadeOutWithDuration:0.3f]];
}

- (void)showCoinsTextAnimation:(NSString *)text
                 numberOfCoins:(NSInteger)numberOfCoins
                        awhile:(BOOL)awhile {
  if (_showingTextAnimation) {
    return;
  }
  _showingTextAnimation = YES;
  
  _coinsTextContainerNode.xScale = 1.0f;
  _coinsTextContainerNode.yScale = 1.0f;
  [_coinsTextContainerNode setText:text
                     numberOfCoins:numberOfCoins];
  _coinsTextContainerNode.position = CGPointMake(0.0f, -50.0f);
  _coinsTextContainerNode.xScale = 0.3f;
  _coinsTextContainerNode.yScale = 0.3f;
  _coinsTextContainerNode.alpha = 0.0f;
  
  [_coinsTextContainerNode removeAllActions];
  [_coinsTextContainerNode runAction:[SKAction group:@[
                                                        [SKAction moveBy:CGVectorMake(0.0f, 75.0f) duration:0.3f],
                                                        [SKAction fadeInWithDuration:0.3f],
                                                        [SKAction scaleTo:1.0f duration:0.3f]
                                                      ]]];
  
  [_textGradientSpriteNode runAction:[SKAction fadeInWithDuration:0.3f]];
  
  if (awhile) {
    [self performSelector:@selector(hideCoinsTextAnimaiton) withObject:nil afterDelay:2.0f];
  }
}

- (void)hideCoinsTextAnimaiton {
  if (_showingTextAnimation == NO) {
    return;
  }
  _showingTextAnimation = NO;
  
  [_coinsTextContainerNode removeAllActions];
  [NSObject cancelPreviousPerformRequestsWithTarget:self];
  [_coinsTextContainerNode runAction:[SKAction group:@[
                                                       [SKAction moveBy:CGVectorMake(0.0f, -75.0f) duration:0.3f],
                                                       [SKAction fadeOutWithDuration:0.3f],
                                                       [SKAction scaleTo:0.3f duration:0.3f]
                                                      ]]];
  
  [_coinsTextContainerNode runAction:[SKAction fadeOutWithDuration:0.3f]];
}

#pragma mark - Overridden

- (void)update:(NSTimeInterval)currentTime {
  [super update:currentTime];
}

@end

//
//  MMMusicLibraryDataSource.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MMMusicLibrarySection) {
  MMMusicLibrarySectionHeader,
  MMMusicLibrarySectionAllSongsAndAssignements
};
#define kMMMusicLibraryNumberOfSections 3

typedef NS_ENUM(NSInteger, MMMusicLibrarySongsSection) {
  MMMusicLibrarySongsSectionAll,
  MMMusicLibrarySongsSectionAssignements
};

@interface MMMusicLibraryDataSource : NSObject < UICollectionViewDataSource >

@property (nonatomic, assign) MMMusicLibrarySongsSection songsSection;
@property (nonatomic, assign) BOOL showRecentlyPlayedMusic;

@end

//
//  MMCountryPicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/17/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MMCountryPickerDelegate;

@interface MMCountryPicker : UIPickerView

@property (nonatomic, weak) id<MMCountryPickerDelegate> countryPickerDelegate;

@property (nonatomic, strong) NSString *selectedCountryName;
@property (nonatomic, strong) NSString *selectedCountryCode;

+ (NSArray *)countryNames;
+ (NSArray *)countryCodes;
+ (NSDictionary *)countryNamesByCode;
+ (NSDictionary *)countryCodesByName;
+ (NSString *)countryNameForCountryCode:(NSString *)countryCode;

@end

@protocol MMCountryPickerDelegate <NSObject>

- (void)countryPicker:(MMCountryPicker *)picker didSelectCountryWithCode:(NSString *)countryCode countryName:(NSString *)countryName;

@end

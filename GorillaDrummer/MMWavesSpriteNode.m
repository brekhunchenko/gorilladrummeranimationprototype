//
//  MMWavesSpriteNode.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/20/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMWavesSpriteNode.h"

@interface MMWavesSpriteNode()

@property (nonatomic, strong) SKShapeNode* waveShapeNode;

@property (nonatomic, assign) CGFloat phase;
@property (nonatomic, assign) CGFloat frequency;

@end

@implementation MMWavesSpriteNode

#pragma mark - Initializers

- (instancetype)initWithSize:(CGSize)size {
  self = [super initWithColor:[UIColor clearColor] size:size];
  if (self) {
    [self _setupWavesNodes];
    
    self.frequency = 1.5f;
  }
  return self;
}

#pragma mark - Setup

- (void)_setupWavesNodes {
  _waveShapeNode = [[SKShapeNode alloc] init];
  _waveShapeNode.fillColor = [[UIColor whiteColor] colorWithAlphaComponent:0.15f];
  _waveShapeNode.strokeColor = [UIColor clearColor];
  _waveShapeNode.path = [UIBezierPath bezierPath].CGPath;
  _waveShapeNode.position = CGPointMake(-self.size.width/2.0f, -self.size.height/2.0f);
  [self addChild:_waveShapeNode];
}

#pragma mark - Public Methods

- (void)updateWithPeakLevel:(CGFloat)peakLevel{
  self.phase += 0.05;
  
  UIBezierPath* bezier = [UIBezierPath bezierPath];
  CGRect bounds = self.scene.frame;
  CGFloat halfHeight = CGRectGetHeight(bounds) / 2.0f;
  CGFloat width = CGRectGetWidth(bounds);
  CGFloat mid = width / 2.0f;

  [bezier moveToPoint:CGPointMake(0.0f, bounds.size.height)];
  for (CGFloat x = 0; x<width + 5.0f; x += 5.0f) {
    CGFloat scaling = -pow(1 / mid * (x - mid), 2) + 1;
    scaling *= scaling*0.5f;
    
    CGFloat t = (9.0f*M_PI *(x/width)*self.frequency + self.phase);
    CGFloat wavesHeight = 500.0f;
    CGFloat y = - peakLevel*scaling*wavesHeight*sin(t) + halfHeight + mid/4.0f;
    
    [bezier addLineToPoint:CGPointMake(x, y)];
  }
  [bezier addLineToPoint:CGPointMake(bounds.size.width, bounds.size.height)];
  _waveShapeNode.path = bezier.CGPath;
}

@end

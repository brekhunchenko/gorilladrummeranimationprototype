//
//  MMGorillaDrummerSprite.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GorillaDrummerDefines.h"

@interface MMGorillaDrummerSprite : SKSpriteNode

- (void)showExpressionAnimation:(GorillaDrummerExpression)expression;
- (void)showBeatAnimation:(GorillaDrummerBeatType)beatType;

@end

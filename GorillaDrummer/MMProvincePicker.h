//
//  MMProvincePicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/17/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMProvince.h"

@protocol MMProvincePickerDelegate;

@interface MMProvincePicker : UIPickerView

@property (nonatomic, weak) id<MMProvincePickerDelegate> provincePickerDelegate;

- (void)setupProvincesForCountryCode:(NSString *)countryCode;
@property (nonatomic, strong, readonly) NSString* countryCode;
@property (nonatomic, strong) MMProvince* province;

+ (NSArray <MMProvince *> *)provincesForCountryCode:(NSString *)countryCode;
+ (MMProvince *)provinceForProvinceShort:(NSString *)provinceShort countryCode:(NSString *)countryCode;

@end

@protocol MMProvincePickerDelegate <NSObject>

- (void)provincePicker:(MMProvincePicker *)provincePicker didSelectProvince:(MMProvince *)province;

@end

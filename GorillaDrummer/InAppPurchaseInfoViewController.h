//
//  TermsOfUseViewController.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 9/13/18.
//  Copyright © 2018 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TermsOfUseLanguage) {
    TermsOfUseLanguageEnglish,
    TermsOfUseLanguageChinese
};

@protocol InAppPurchaseInfoViewControllerDelegate;

@interface InAppPurchaseInfoViewController : UIViewController

@property (nonatomic, weak) id<InAppPurchaseInfoViewControllerDelegate> delegate;

@property (nonatomic, assign, readonly) TermsOfUseLanguage language;

@end

@protocol InAppPurchaseInfoViewControllerDelegate <NSObject>

- (void)inAppPurchaseInfoViewControllerDidAskToShowTermsOfUse:(InAppPurchaseInfoViewController *)vc;
- (void)inAppPurchaseInfoViewControllerDidAskToShowPrivacyPolicy:(InAppPurchaseInfoViewController *)vc;
- (void)inAppPurchaseInfoViewControllerDidAskToShowNewMonthlySubscription:(InAppPurchaseInfoViewController *)vc;
- (void)inAppPurchaseInfoViewControllerDidAskToShowAnnualSubscription:(InAppPurchaseInfoViewController *)vc;
- (void)inAppPurchaseInfoViewControllerDidAskToRestorePurchases:(InAppPurchaseInfoViewController *)vc;
- (void)inAppPurchaseInfoViewControllerDidAskToContinueAsGuest:(InAppPurchaseInfoViewController *)vc;

@end


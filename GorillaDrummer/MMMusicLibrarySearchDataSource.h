//
//  MMMusicLibrarySearchDataSource.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MMMusicLibrarySearchDataSource : NSObject < UICollectionViewDataSource >

- (void)updateDataSourceForSearchKeyword:(NSString *)searchKeyword
                       instrumentsFilter:(NSString *)instrumentsFilter
                              keysFilter:(NSString *)keysFilter;

@property (nonatomic, strong, readonly) NSArray<NSString *>* listOfInstrumentsAvailable;
@property (nonatomic, strong, readonly) NSArray<NSString *>* listOfKeysAvailable;

@end

//
//  MMCreateAccountViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/25/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMCreateAccountViewController.h"
#import "MMSignUpTextField.h"
#import "NSString+rev.h"
#import "MMCountryPicker.h"
#import "MMProvincePicker.h"
#import "MMRoundedContainerView.h"
#import "MMPhoneCodePicker.h"
#import "MMLocationManager.h"
#import "MMCityPicker.h"
#import "MMInstrumentPicker.h"
#import "MMGradingExamPicker.h"
#import "MMGradePicker.h"
#import "MMInstrumentPicker.h"

@interface MMCreateAccountViewController () < UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, MMCountryPickerDelegate, MMProvincePickerDelegate, MMPhoneCodePickerDelegate, MMCityPickerDelegate, MMInstrumentsPickerDelegate, MMGradingExamPickerDelegate, MMGradePickerDelegate >

@property (weak, nonatomic) IBOutlet MMSignUpTextField *gradingExamTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *gradeTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *firstnameTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *userIDTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *confirmPassowrdTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *countryTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *provinceTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *cityTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *teacherReferenceCodeTextfield;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *instrumentsTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *usernameTextField;
@property (weak, nonatomic) IBOutlet MMRoundedContainerView *provinceContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UILabel *errorAlertLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *errorAlertContainerViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIView *errorAlertContainerView;
@property (nonatomic, strong) MMCountryPicker* countryPicker;
@property (nonatomic, strong) MMProvincePicker* provincePicker;
@property (nonatomic, strong) MMCityPicker* cityPicker;
@property (nonatomic, strong) MMGradingExamPicker* gradingExamPicker;
@property (nonatomic, strong) MMGradePicker* gradePicker;
@property (nonatomic, strong) MMInstrumentPicker* instrumentPicker;

@end

@implementation MMCreateAccountViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"Create Account";
  
  //NOTE: TEST
  _signUpButton.alpha = 1.0f;
  _signUpButton.userInteractionEnabled = YES;
  
  [self _setupTextFields];
  [self _setupAlertContainerView];
  [self _setupImageViews];
  [self _setupNavigationItems];
    
  [[MMLocationManager sharedManager] refreshLocation];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(_prefillTextfieldsWithLocation:)
                                               name:kLocationManagerDidChangeLocationNotification
                                             object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Setup

- (void)_setupImageViews {
  [self.view layoutIfNeeded];
  
  _avatarImageView.layer.cornerRadius = _avatarImageView.bounds.size.width/2.0f;
  _avatarImageView.layer.masksToBounds = YES;
}

- (void)_setupTextFields {
  _userIDTextField.text = _userID;
    
  _countryPicker = [MMCountryPicker new];
  _countryPicker.countryPickerDelegate = self;
  _countryTextField.inputView = _countryPicker;
  _countryTextField.inputAccessoryView = [self _toolbarWithTitle:NSLocalizedString(@"Please select your country:", nil)];
  _countryTextField.inputAssistantItem.leadingBarButtonGroups = @[];
  _countryTextField.inputAssistantItem.trailingBarButtonGroups = @[];
  
  _provincePicker = [MMProvincePicker new];
  _provincePicker.provincePickerDelegate = self;
  _provinceTextField.inputView = _provincePicker;
  _provinceTextField.inputAccessoryView = [self _toolbarWithTitle:NSLocalizedString(@"Please select your province:", nil)];
  _provinceTextField.inputAssistantItem.leadingBarButtonGroups = @[];
  _provinceTextField.inputAssistantItem.trailingBarButtonGroups = @[];
   
  _cityPicker = [MMCityPicker new];
  _cityPicker.cityPickerDelegate = self;
  _cityTextField.inputView = _cityPicker;
  _cityTextField.inputAccessoryView = [self _toolbarWithTitle:NSLocalizedString(@"Please select your city:", nil)];
  _cityTextField.inputAssistantItem.leadingBarButtonGroups = @[];
  _cityTextField.inputAssistantItem.trailingBarButtonGroups = @[];
  
  _gradingExamPicker = [MMGradingExamPicker new];
  _gradingExamPicker.gradingExamDelegate = self;
  _gradingExamPicker.showGradientOvelay = NO;
  
  _gradePicker = [MMGradePicker new];
  _gradePicker.gradePickerDelegate = self;
  _gradePicker.showGradientOvelay = NO;
  
  _instrumentPicker = [[MMInstrumentPicker alloc] init];
  _instrumentPicker.instrumentsPickerDelegate = self;
}

- (UIToolbar *)_toolbarWithTitle:(NSString *)title {
  UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 44.0f)];
  toolBar.barStyle = UIBarStyleDefault;
  toolBar.translucent = YES;
  UILabel* titleLabel = [[UILabel alloc] initWithFrame:toolBar.bounds];
  titleLabel.textAlignment = NSTextAlignmentCenter;
  titleLabel.text = title;
  [toolBar addSubview:titleLabel];
  UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self
                                                                action:@selector(donePickerViewButtonAction:)];
  [toolBar setItems:@[doneButton]];
  return toolBar;
}

- (void)_setupAlertContainerView {
  _errorAlertContainerView.layer.cornerRadius = 10.0f;
}

- (void)_setupNavigationItems {
  UIImage* backButtonImage = [[UIImage imageNamed:@"BtnBackAuth"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
  UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.imageView.contentMode = UIViewContentModeCenter;
  backButton.frame = CGRectMake(0.0f, 0.0f, 84.0f, 84.0f);
  [backButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0, 0.0f)];
  [backButton setImage:backButtonImage forState:UIControlStateNormal];
  [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem* cardBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
  cardBarButtonItem.tintColor = [UIColor redColor];
  self.navigationItem.leftBarButtonItem = cardBarButtonItem;
}

#pragma mark - Actions

- (IBAction)signUpButtonAction:(id)sender {
  [self.view endEditing:YES];
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)avatarButtonAction:(id)sender {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Choose a Source" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
  
  UIAlertAction* takePhoto = [UIAlertAction actionWithTitle:@"Take a photo"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                      
                                                      if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                                        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
                                                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                        picker.delegate = self;
                                                        picker.modalPresentationStyle = UIModalPresentationPopover;
                                                        picker.popoverPresentationController.sourceView = sender;
                                                        picker.popoverPresentationController.sourceRect = self.view.bounds;
                                                        [self presentViewController:picker animated:YES completion:NULL];
                                                      }
                                                    }];
  UIAlertAction* choosePicture = [UIAlertAction actionWithTitle:@"Choose from gallery"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                          
                                                          UIImagePickerController* picker = [[UIImagePickerController alloc] init];
                                                          picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                          picker.delegate = self;
                                                          picker.modalPresentationStyle = UIModalPresentationPopover;
                                                          picker.popoverPresentationController.sourceView = sender;
                                                          picker.popoverPresentationController.sourceRect = CGRectMake(-170.0f, -250.0f, 320.0f, 640.0f);
                                                          picker.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
                                                          picker.allowsEditing = YES;
                                                          [self presentViewController:picker animated:YES completion:NULL];
                                                        }];
  
  UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleCancel
                                                 handler:nil];
  
  [alertController addAction:takePhoto];
  [alertController addAction:choosePicture];
  [alertController addAction:cancel];
  
  alertController.popoverPresentationController.sourceView = sender;
  alertController.popoverPresentationController.sourceRect = CGRectMake(-170.0f, -250.0f, 320.0f, 640.0f);
  alertController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
  
  [self presentViewController:alertController animated:YES completion:nil];
}

- (void)backButtonAction:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)donePickerViewButtonAction:(id)sender {
  [self.view endEditing:YES];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
  __unused UIImage *image = info[UIImagePickerControllerEditedImage];
  //TODO: Save image.
  _avatarImageView.image = image;
  [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [self.view endEditing:YES];
  
  UITextField* nextTextField = [self.view viewWithTag:(textField.tag + 1)];
  [nextTextField becomeFirstResponder];
  
  return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
  if (textField == _instrumentsTextField) {
    _instrumentPicker.modalPresentationStyle = UIModalPresentationPopover;
    _instrumentPicker.preferredContentSize = CGSizeMake(200.0f, 400.0);
    _instrumentPicker.popoverPresentationController.sourceView = _instrumentsTextField;
    _instrumentPicker.popoverPresentationController.sourceRect = CGRectOffset(_instrumentsTextField.frame, -16.0, 0.0);
    _instrumentPicker.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    [self presentViewController:_instrumentPicker animated:YES completion:nil];
    
    return NO;
  } else if (textField == _gradeTextField) {
    _gradePicker.modalPresentationStyle = UIModalPresentationPopover;
    _gradePicker.preferredContentSize = CGSizeMake(200.0f, 475.0);
    _gradePicker.popoverPresentationController.sourceView = _gradeTextField;
    _gradePicker.popoverPresentationController.sourceRect = CGRectOffset(_gradeTextField.frame, -16.0, 0.0);
    _gradePicker.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    [self presentViewController:_gradePicker animated:YES completion:nil];
    
    return NO;
  } else if (textField == _gradingExamTextField) {
    _gradingExamPicker.modalPresentationStyle = UIModalPresentationPopover;
    _gradingExamPicker.preferredContentSize = CGSizeMake(200.0f, 180.0);
    _gradingExamPicker.popoverPresentationController.sourceView = _gradingExamTextField;
    _gradingExamPicker.popoverPresentationController.sourceRect = CGRectOffset(_gradingExamTextField.frame, -16.0, 0.0);
    _gradingExamPicker.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    [self presentViewController:_gradingExamPicker animated:YES completion:nil];
    
    return NO;
  }
  return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
  __unused NSError* error = [self _checkForTextfieldsError];
  if (error == nil) {
    _signUpButton.alpha = 1.0f;
    _signUpButton.userInteractionEnabled = YES;
  } else {
    _signUpButton.alpha = 0.5f;
    _signUpButton.userInteractionEnabled = NO;
  }
  return YES;
}

#pragma mark - Private Methods

- (NSError *)_checkForTextfieldsError {
  if ([_passwordTextField.text isEqualToString:_confirmPassowrdTextField.text] == NO || _passwordTextField.text.length < 5) {
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Passwords do not match. Please try again."}];
  } else if (_firstnameTextField.text.length == 0) {
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Please enter your first name."}];
  } else if (_firstnameTextField.text.length == 0) {
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Please enter your last name."}];
  } else if (_countryTextField.text.length == 0) {
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Please select your country."}];
  } else if (_provinceTextField.text.length == 0) {
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Please select your provience."}];
  } else if (_cityTextField.text.length == 0) {
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Please select your city."}];
  }
  return nil;
}

- (void)_showErrorAlertWithText:(NSString *)errorSummary {
  _errorAlertLabel.text = errorSummary;
  
  [UIView animateWithDuration:0.5f animations:^{
    _errorAlertContainerViewTopSpaceConstraint.constant = 4.0f;
    [self.view layoutIfNeeded];
  }];
}

- (void)_hideErrorAlertAnimated {
  [UIView animateWithDuration:0.5f animations:^{
    _errorAlertContainerViewTopSpaceConstraint.constant = -_errorAlertContainerView.bounds.size.height;
    [self.view layoutIfNeeded];
  }];
}

#pragma mark - MMCountryPickerDelegate

- (void)countryPicker:(MMCountryPicker *)picker didSelectCountryWithCode:(NSString *)countryCode countryName:(NSString *)countryName {
  [self _selectCountryWithCountryCode:countryCode countryName:countryName];
}

- (void)_selectCountryWithCountryCode:(NSString *)countryCode countryName:(NSString *)countryName {
  [_provincePicker setupProvincesForCountryCode:countryCode];
  [_cityPicker setupCitiesForCountryName:countryName];
  
  _countryTextField.text = countryName;
  if ([MMProvincePicker provincesForCountryCode:countryCode].count == 0) {
    _provinceContainerView.alpha = 0.5f;
    _provinceContainerView.userInteractionEnabled = NO;
  } else {
    _provinceContainerView.alpha = 1.0f;
    _provinceContainerView.userInteractionEnabled = YES;
  }
}

#pragma mark - MMProvincePickerDelegate

- (void)provincePicker:(MMProvincePicker *)provincePicker didSelectProvince:(NSString *)province {
  _provinceTextField.text = province;
}

#pragma amrk - MMCityPickerDelegate

- (void)cityPicker:(MMCityPicker *)cityPicker didSelectCity:(MMCity *)city {
  _cityTextField.text = city.cityName;
}

#pragma mark - MMInstrumentsPickerDelegate

- (void)instrumentsPicker:(MMInstrumentPicker *)instrumentsPicker didSelectInstrument:(NSString *)instrument {
  _instrumentsTextField.text = instrument;
  _instrumentPicker.instrument = instrument;
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MMGradingExamPickerDelegate

- (void)gradingExamPicker:(MMGradingExamPicker *)picker didSelectGradingExam:(NSString *)gradingExam {
  _gradingExamTextField.text = gradingExam;
  _gradingExamPicker.gradingExam = gradingExam;
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MMGradePickerDelegate

- (void)gradePicker:(MMGradePicker *)gradePicker didSelectGrade:(NSString *)grade {
  _gradeTextField.text = grade;
  _gradePicker.grade = grade;
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Notifications

- (void)_prefillTextfieldsWithLocation:(NSNotification *)notification {
  if (notification.object) {
    CLLocation* currentLocation = [notification object];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
       if (error == nil && [placemarks count] > 0) {
         CLPlacemark *placemark = [placemarks lastObject];
         NSString* countryCode = placemark.ISOcountryCode;
         _countryPicker.selectedCountryCode = countryCode;
         _countryTextField.text = _countryPicker.selectedCountryName;
  
         [_provincePicker setupProvincesForCountryCode:countryCode];
         [_cityPicker setupCitiesForCountryName:placemark.country];
           
         _provincePicker.province = [MMProvincePicker provinceForProvinceShort:placemark.administrativeArea countryCode:_provincePicker.countryCode];
         _provinceTextField.text = _provincePicker.province.province;
         
         _cityTextField.text = placemark.locality;
         _cityPicker.city = [MMCityPicker cityForCityName:placemark.locality countyName:placemark.country];
       }
     }];
  }
}

@end

//
//  MMGradientView.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/20/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMGradientView.h"

@implementation MMGradientView {
  CGGradientRef gradient;
}

#pragma mark - Public Methods

- (void)updateGradientWithTopColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor {
  NSParameterAssert(topColor);
  NSParameterAssert(bottomColor);
  
  _topColor = topColor;
  _bottomColor = bottomColor;
  
  [self _setupGradient];
  [self setNeedsDisplay];
}

#pragma mark - Private Methods

- (void)_setupGradient {
  if (_topColor && _bottomColor) {
    CGFloat r0, b0, g0;
    [_topColor getRed:&r0 green:&g0 blue:&b0 alpha:NULL];
    
    CGFloat r1, b1, g1;
    [_bottomColor getRed:&r1 green:&g1 blue:&b1 alpha:NULL];
    
    
    CGFloat colors[] = {
      r0,g0,b0,1.0,
      r1,g1,b1,1.0
    };
    
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    
    if (gradient != nil) {
      CGGradientRelease(gradient);
    }
    gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    CGColorSpaceRelease(rgb);
  }
}

#pragma mark - Overridden

- (void)drawRect:(CGRect)rect {
  if (gradient) {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect clippingRect = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
    
    CGPoint endPoints[] = {
      CGPointMake(0.0f, 0.0f),
      CGPointMake(0.0f, self.frame.size.height),
    };
    
    CGContextSaveGState(context);
    CGContextClipToRect(context, clippingRect);
    CGContextDrawLinearGradient(context, gradient, endPoints[0], endPoints[1], 0);
    CGContextRestoreGState(context);
  }
}

#pragma mark - 

- (void)dealloc {
  CGGradientRelease(gradient);
  _topColor = nil;
  _bottomColor = nil;
}

@end

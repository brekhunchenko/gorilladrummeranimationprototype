//
//  MMWavesSpriteNode.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/20/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MMWavesSpriteNode : SKSpriteNode

- (instancetype)initWithSize:(CGSize)size;

- (void)updateWithPeakLevel:(CGFloat)peakLevel;

@end

//
//  NSString+rev.h
//  RealEstateVids
//
//  Created by Alex on 3/27/15.
//  Copyright (c) 2015 itomych. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (rev)

+ (NSString*)generateUniqueFileNameWithExtension:(NSString*)extension;
+ (NSString*)uniqueFilePathsInDocumentsDirectoryWithName:(NSString*)name withExtension:(NSString*)extension;
+ (NSString*)filePathInDocumentsDirectoryWithName:(NSString*)name withExtension:(NSString*)extension;
+ (NSString *)applicationDocumentsDirectory;
+ (BOOL)validateEmailString:(NSString *)email;
+ (BOOL)validatePhone:(NSString *)phoneNumber;
- (NSString *)re_stringByReplacingOccurrencesOfString:(NSString *)target withString:(NSString *)replacement;

@end

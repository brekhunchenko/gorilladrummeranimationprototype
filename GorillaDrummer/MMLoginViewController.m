//
//  MMLoginViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/27/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMLoginViewController.h"
#import "MMSignUpTextField.h"
#import "MMCreateUserIDViewController.h"
#import "MMRoundedContainerView.h"
#import "MMPhoneCodePicker.h"
#import "MMLocationManager.h"
#import "InAppPurchaseInfoViewController.h"

@interface MMLoginViewController () < MMPhoneCodePickerDelegate >

@property (weak, nonatomic) IBOutlet UILabel *errorAlertLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *errorAlertContainerViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIView *errorAlertContainerView;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *emailTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIView *bottomContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContainerViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *darkBackgroundImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopImageViewConstraint;
@property (weak, nonatomic) IBOutlet UISegmentedControl *userIDSegmentedControl;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *phoneCodeTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIView *phoneNumberContainerView;
@property (weak, nonatomic) IBOutlet MMRoundedContainerView *emailContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (nonatomic, strong) MMPhoneCodePicker* phoneCodePicker;

@end

typedef NS_ENUM(NSInteger, MMLoginViewControllerUserIDType) {
    MMLoginViewControllerUserIDTypeEmail,
    MMLoginViewControllerUserIDTypeMobile
};

@implementation MMLoginViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self _setupAlertContainerView];
    [self _setupSegmenetedControl];
    [self _setupTextFields];
    
    [[MMLocationManager sharedManager] refreshLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_prefillTextfieldsWithLocation:)
                                                 name:kLocationManagerDidChangeLocationNotification
                                               object:nil];

}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  [self _showInitialAnimation];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

#pragma mark - Setup


- (void)_setupSegmenetedControl {
    [_userIDSegmentedControl setTitleTextAttributes:@{
                                                      NSFontAttributeName : [UIFont systemFontOfSize:15.0f],
                                                      NSForegroundColorAttributeName : [UIColor whiteColor]
                                                      }
                                           forState:UIControlStateNormal];
    [_userIDSegmentedControl setTitleTextAttributes:@{
                                                      NSFontAttributeName : [UIFont systemFontOfSize:15.0f],
                                                      NSForegroundColorAttributeName : [UIColor whiteColor]
                                                      }
                                           forState:UIControlStateSelected];
}

- (void)_setupTextFields {
    _phoneCodePicker = [MMPhoneCodePicker new];
    _phoneCodePicker.phoneCodePickerDelegate = self;
    _phoneCodeTextField.inputView = _phoneCodePicker;
    _phoneCodeTextField.inputAccessoryView = [self _toolbarWithTitle:NSLocalizedString(@"Please select your phone code:", nil)];
    _phoneCodeTextField.inputAssistantItem.leadingBarButtonGroups = @[];
    _phoneCodeTextField.inputAssistantItem.trailingBarButtonGroups = @[];
}

- (void)_setupAlertContainerView {
  _errorAlertContainerView.layer.cornerRadius = 10.0f;
}

#pragma mark - Actions

- (IBAction)loginButtonAction:(id)sender {
    UINavigationController* navController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                               instantiateViewControllerWithIdentifier:@"MMMusicLibraryViewControllerNavigation"];
    navController.modalPresentationStyle = UIModalPresentationFullScreen;
    self.view.window.rootViewController = navController;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        InAppPurchaseInfoViewController* vc = [[InAppPurchaseInfoViewController alloc] init];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    });
}

- (IBAction)googleButtonAction:(id)sender {
  MMCreateUserIDViewController* vc = [[UIStoryboard storyboardWithName:@"Auth" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([MMCreateUserIDViewController class])];
  [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)facebookButtonAction:(id)sender {
  //TODO:
}

- (IBAction)weChatButtonAction:(id)sender {
  //TODO:
}

- (IBAction)qqButtonAction:(id)sender {
  //TODO:
}

- (IBAction)helpWithAccountButtonAction:(id)sender {
  //TODO:
  [self _showErrorAlertWithText:@"test"];
  [self performSelector:@selector(_hideErrorAlertAnimated)
             withObject:nil
             afterDelay:3.0f];
}

- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender {
    MMLoginViewControllerUserIDType userIDType = (MMLoginViewControllerUserIDType)sender.selectedSegmentIndex;
    if (userIDType == MMLoginViewControllerUserIDTypeEmail) {
        _emailContainerView.hidden = NO;
        _phoneNumberContainerView.hidden = YES;
    } else {
        _emailContainerView.hidden = YES;
        _phoneNumberContainerView.hidden = NO;
    }
    
    [self.view endEditing:YES];
}

- (void)donePickerViewButtonAction:(id)sender {
    [self.view endEditing:YES];
    
    [_phoneNumberTextField becomeFirstResponder];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [self.view endEditing:YES];
  
  if (textField == _emailTextField || textField == _phoneNumberContainerView) {
      [_passwordTextField becomeFirstResponder];
  }
  
  return YES;
}

#pragma mark - Private Methods

- (UIToolbar *)_toolbarWithTitle:(NSString *)title {
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 44.0f)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.translucent = YES;
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:toolBar.bounds];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [toolBar addSubview:titleLabel];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(donePickerViewButtonAction:)];
    [toolBar setItems:@[doneButton]];
    return toolBar;
}

- (void)_showInitialAnimation {
  [UIView animateWithDuration:0.5f animations:^{
    _bottomContainerViewBottomConstraint.constant = 0.0f;
    _logoTopImageViewConstraint.constant = 50.0;
    _bottomContainerView.alpha = 1.0f;
    _darkBackgroundImageView.alpha = 1.0f;
    
    [self.view layoutIfNeeded];
  }];
}

- (void)_showErrorAlertWithText:(NSString *)errorSummary {
  _errorAlertLabel.text = errorSummary;
  
  [UIView animateWithDuration:0.5f animations:^{
    _errorAlertContainerViewTopSpaceConstraint.constant = -10.0f;
    [self.view layoutIfNeeded];
  }];
}

- (void)_hideErrorAlertAnimated {
  [UIView animateWithDuration:0.5f animations:^{
    _errorAlertContainerViewTopSpaceConstraint.constant = -_errorAlertContainerView.bounds.size.height;
    [self.view layoutIfNeeded];
  }];
}

#pragma mark - MMPhoneCodePickerDelegate

- (void)phoneCodePicker:(MMPhoneCodePicker *)picker didSelectPhoneCode:(MMCountryPhoneCode *)phoneCode {
    _phoneCodeTextField.text = [NSString stringWithFormat:@"+%@", phoneCode.phoneCode];
    _flagImageView.image = [UIImage imageNamed:phoneCode.countryCode];
}

#pragma mark - Notifications

- (void)_prefillTextfieldsWithLocation:(NSNotification *)notification {
    if (notification.object) {
        CLLocation* currentLocation = [notification object];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil && [placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks lastObject];
                NSString* countryCode = placemark.ISOcountryCode;
                _phoneCodePicker.selectedPhoneCode = [MMPhoneCodePicker phoneCodeForCountryCode:countryCode];
                _phoneCodeTextField.text = [NSString stringWithFormat:@"+%@", _phoneCodePicker.selectedPhoneCode.phoneCode];
                _flagImageView.image = [UIImage imageNamed:countryCode];
            }
        }];
    }
}

@end

//
//  MMSignUpTextField.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/25/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMSignUpTextField.h"

@implementation MMSignUpTextField

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _initialSetup];
}

#pragma mark - Setup

- (void)_initialSetup {
    NSAttributedString* attributedPlaceholder = [[NSAttributedString alloc] initWithString:(self.placeholder == nil ? @"" : self.placeholder)
                                                                              attributes:@{
                                                                                           NSFontAttributeName : [UIFont systemFontOfSize:18.0f],
                                                                                           NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.6f]
                                                                                           }];
  self.attributedPlaceholder = attributedPlaceholder;
  
  self.textColor = [UIColor whiteColor];
  self.font = [UIFont systemFontOfSize:18.0f];
}

@end

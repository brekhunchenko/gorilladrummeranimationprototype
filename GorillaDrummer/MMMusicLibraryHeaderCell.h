//
//  MMMusicLibraryHeaderCell.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MMMusicLibraryHeaderCellDelegate;

@interface MMMusicLibraryHeaderCell : UICollectionViewCell

@property (nonatomic, weak) id<MMMusicLibraryHeaderCellDelegate> delegate;

@property (nonatomic, assign) NSInteger numberOfAvailableAssignments;
@property (nonatomic, assign) BOOL showRecentlyPlayed;

+ (CGFloat)cellHeight:(BOOL)showRecentlyPlayed;

@end

@protocol MMMusicLibraryHeaderCellDelegate <NSObject>

- (void)musicLibraryHeaderCellAskToShowAssignments:(MMMusicLibraryHeaderCell *)cell;
- (void)musicLibraryHeaderCellAskToShowAllSongs:(MMMusicLibraryHeaderCell *)cell;

@end

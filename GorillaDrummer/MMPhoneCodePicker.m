//
//  MMPhoneCodePicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMPhoneCodePicker.h"

@interface MMPhoneCodePicker() <UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation MMPhoneCodePicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupProvinces];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupProvinces];
}

#pragma mark - Setup

- (void)_setupProvinces {
  self.dataSource = self;
  self.delegate = self;
}

#pragma mark - Custom Setters & Getters

- (void)setSelectedPhoneCode:(MMCountryPhoneCode *)selectedPhoneCode {
  _selectedPhoneCode = selectedPhoneCode;
  
  NSArray<MMCountryPhoneCode *> *phoneCodes = [self.class listOfCountryPhoneCodes];
  __block NSInteger indexOfPhoneCode = NSNotFound;
  [phoneCodes enumerateObjectsUsingBlock:^(MMCountryPhoneCode * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    if ([obj.phoneCode isEqualToString:_selectedPhoneCode.phoneCode]) {
      *stop = YES;
      indexOfPhoneCode = idx;
    }
  }];
  
  if (indexOfPhoneCode != NSNotFound) {
    [self selectRow:indexOfPhoneCode inComponent:0 animated:NO];
  }
}

#pragma mark - UIPickerView

- (NSInteger)numberOfComponentsInPickerView:(__unused UIPickerView *)pickerView {
  return 1;
}

- (NSInteger)pickerView:(__unused UIPickerView *)pickerView numberOfRowsInComponent:(__unused NSInteger)component {
  return (NSInteger)[[self class] listOfCountryPhoneCodes].count;
}

- (UIView *)pickerView:(__unused UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(__unused NSInteger)component reusingView:(UIView *)view {
  if (!view) {
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
    label.backgroundColor = [UIColor clearColor];
    label.tag = 1;
    [view addSubview:label];
    
    UIImageView *flagView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
    flagView.contentMode = UIViewContentModeScaleAspectFit;
    flagView.tag = 2;
    [view addSubview:flagView];
  }
  
  MMCountryPhoneCode* phoneCode = [[self class] listOfCountryPhoneCodes][(NSUInteger) row];
  ((UILabel *)[view viewWithTag:1]).text = [NSString stringWithFormat:@"(%@) +%@", phoneCode.countryCode, phoneCode.phoneCode];
  UIImage *image = [UIImage imageNamed:phoneCode.countryCode];
  ((UIImageView *)[view viewWithTag:2]).image = image;
  return view;
}

- (void)pickerView:(__unused UIPickerView *)pickerView
      didSelectRow:(__unused NSInteger)row
       inComponent:(__unused NSInteger)component {
  MMCountryPhoneCode* phoneCode =  [[self class] listOfCountryPhoneCodes][(NSUInteger)row];
  if ([self.phoneCodePickerDelegate respondsToSelector:@selector(phoneCodePicker:didSelectPhoneCode:)]) {
    [self.phoneCodePickerDelegate phoneCodePicker:self didSelectPhoneCode:phoneCode];
  }
}

#pragma mark - Class Methods

+ (NSArray<MMCountryPhoneCode *> *)listOfCountryPhoneCodes {
  static NSArray* phoneCodes = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"phone_country_code" ofType:@"json"];
    NSError * error = nil;
    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    NSData * jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    
    NSMutableArray* phoneCodesMutable = [NSMutableArray new];
    for (NSDictionary* dict in parsedData) {
      MMCountryPhoneCode* phoneCode = [[MMCountryPhoneCode alloc] initWithDictionary:dict];
      [phoneCodesMutable addObject:phoneCode];
    }
        
    phoneCodes = phoneCodesMutable;
  });
  
  return phoneCodes;
}

+ (MMCountryPhoneCode *)phoneCodeForCountryCode:(NSString *)countryCode {
  NSArray<MMCountryPhoneCode *> *phoneCodes = [self.class listOfCountryPhoneCodes];
  __block NSInteger indexOfPhoneCode = NSNotFound;
  [phoneCodes enumerateObjectsUsingBlock:^(MMCountryPhoneCode * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    if ([obj.countryCode isEqualToString:countryCode]) {
      *stop = YES;
      indexOfPhoneCode = idx;
    }
  }];
  
  if (indexOfPhoneCode != NSNotFound) {
    return phoneCodes[indexOfPhoneCode];
  } else {
    return nil;
  }
}

@end

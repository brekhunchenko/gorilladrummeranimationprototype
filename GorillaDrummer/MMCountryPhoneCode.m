//
//  MMCountryPhoneCode.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMCountryPhoneCode.h"

@implementation MMCountryPhoneCode

#pragma mark - Initializers

- (instancetype)initWithDictionary:(NSDictionary *)dict {
  self = [super init];
  if (self) {
    _phoneCode = [[dict objectForKey:@"phone_code"] stringValue];
    _countryCode = [dict objectForKey:@"country_code"];
  }
  return self;
}

@end

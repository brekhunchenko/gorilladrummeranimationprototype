//
//  MMRoundedContainerView.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/25/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMRoundedContainerView.h"

@implementation MMRoundedContainerView

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _initialSetup];
}

#pragma mark - Setup

- (void)_initialSetup {
  [self layoutIfNeeded];
  
  self.layer.cornerRadius = self.bounds.size.height/5.0f;
  self.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6f].CGColor;
  self.layer.borderWidth = 1.5f;
  
  self.backgroundColor = [UIColor clearColor];
}

@end

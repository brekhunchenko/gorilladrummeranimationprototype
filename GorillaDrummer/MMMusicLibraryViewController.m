//
//  MMMusicLibraryViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMMusicLibraryViewController.h"
#import "MMCoinsBarButtonItem.h"
#import "MMGorillaDrummerViewController.h"
#import "MMUserProfileViewController.h"
#import "MMRoundedButton.h"
#import "MMUserStatsViewController.h"
#import "MMMusicLibraryDataSource.h"
#import "MMMusicLibraryHeaderCell.h"
#import "MMMusicLibrarySongCell.h"
#import "MMMusicLibrarySearchDataSource.h"
#import "MMMusicLibrarySearchFilterPickerViewController.h"

@interface MMMusicLibraryViewController () < UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MMMusicLibraryHeaderCellDelegate, UITextFieldDelegate, MMMusicLibrarySearchFilterPickerViewControllerDelegate, MMSwipeCollectionViewCellDelegate >

@property (nonatomic, strong) MMMusicLibraryDataSource* musicLibraryDataSource;
@property (weak, nonatomic) IBOutlet UICollectionView *musicLibraryCollectionView;

@property (nonatomic, strong) MMMusicLibrarySearchDataSource* searchDataSource;
@property (weak, nonatomic) IBOutlet UICollectionView *searchCollectionView;

@property (weak, nonatomic) IBOutlet MMRoundedButton *instrumentsFilterButton;
@property (weak, nonatomic) IBOutlet MMRoundedButton *keyFilterButton;
@property (weak, nonatomic) IBOutlet UIView *searchTextfieldContainerView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextfield;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTextfieldContainerViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UIButton *doneSearchButton;

@property (nonatomic, strong) MMMusicLibrarySearchFilterPickerViewController* instrumentsFilterPickerViewController;
@property (nonatomic, strong) MMMusicLibrarySearchFilterPickerViewController* keysFilterPickerViewController;

@property (nonatomic, assign) BOOL searchMode;

@end

@implementation MMMusicLibraryViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Music Library";

    self.navigationController.navigationBar.shadowImage = nil;

    [self.view layoutIfNeeded];
    
    [self _setupBarButtonItems];
    [self _setupSearchContainer];
    [self _setupCollectionViews];
    [self _setupSearchFiltersPickersController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Setup

- (void)_setupBarButtonItems {
    UIImage* profileImage = [[UIImage imageNamed:@"Icon-Avatar"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem* profileBarButtonItem = [[UIBarButtonItem alloc] initWithImage:profileImage
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(profileButtonAction:)];
    self.navigationItem.leftBarButtonItem = profileBarButtonItem;

    MMCoinsBarButtonItem* coinsBarButtonItem = [[MMCoinsBarButtonItem alloc] initWithTarget:self
                                                                                   action:@selector(coinsButtonAction:)];
    coinsBarButtonItem.numberOfCoinsAvailable = 353;
    self.navigationItem.rightBarButtonItem = coinsBarButtonItem;
}

- (void)_setupSearchContainer {
  _searchTextfieldContainerView.layer.cornerRadius = 30.0f;
  _searchTextfieldContainerView.layer.borderWidth = 1.5f;
  _searchTextfieldContainerView.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6f].CGColor;
  
  [_searchTextfield setAttributedPlaceholder:[self _searchPlaceholderString]];
  _searchTextfield.delegate = self;
  [_searchTextfield addTarget:self
                       action:@selector(_searchTextfieldDidChangeText:)
             forControlEvents:UIControlEventEditingChanged];
}

- (void)_setupCollectionViews {
    _musicLibraryDataSource = [[MMMusicLibraryDataSource alloc] init];
    _musicLibraryDataSource.showRecentlyPlayedMusic = NO;
    _musicLibraryCollectionView.dataSource = _musicLibraryDataSource;

    _searchDataSource = [[MMMusicLibrarySearchDataSource alloc] init];
    _searchCollectionView.dataSource = _searchDataSource;
}

- (void)_setupSearchFiltersPickersController {
    CGSize filterControllersSize = CGSizeMake(200.0f, 400.0f);
    NSArray<NSString *>* listOfInstruments = _searchDataSource.listOfInstrumentsAvailable;
    CGRect instrumentsFilterPickerViewFrame = CGRectMake(0.0f,
                                                         CGRectGetMaxY(_instrumentsFilterButton.frame) + 16.0f,
                                                         filterControllersSize.width,
                                                         filterControllersSize.height);
    _instrumentsFilterPickerViewController = [MMMusicLibrarySearchFilterPickerViewController musicLibrarySearchFilterPickerViewController:      instrumentsFilterPickerViewFrame
                                                                                                                            listOfStrings:listOfInstruments];
    _instrumentsFilterPickerViewController.preferredContentSize = filterControllersSize;
    _instrumentsFilterPickerViewController.delegate = self;
    
    NSArray<NSString *>* listOfKeys = _searchDataSource.listOfKeysAvailable;
    CGRect keysFilterPickerViewFrame = CGRectMake(0.0f,
                                                  CGRectGetMaxY(_keyFilterButton.frame) + 16.0f,
                                                  filterControllersSize.width,
                                                  filterControllersSize.height);
    _keysFilterPickerViewController = [MMMusicLibrarySearchFilterPickerViewController musicLibrarySearchFilterPickerViewController:keysFilterPickerViewFrame
                                                                                                                     listOfStrings:listOfKeys];
    _keysFilterPickerViewController.preferredContentSize = filterControllersSize;
    _keysFilterPickerViewController.delegate = self;
}

#pragma mark - UICollectionView Layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
    sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _musicLibraryCollectionView) {
        if (indexPath.section == MMMusicLibrarySectionHeader) {
          return CGSizeMake(collectionView.bounds.size.width, [MMMusicLibraryHeaderCell cellHeight:_musicLibraryDataSource.showRecentlyPlayedMusic]);
        } else {
          return CGSizeMake(collectionView.bounds.size.width - 128.0, [MMMusicLibrarySongCell cellHeight]);
        }
    } else {
        return CGSizeMake(300.0f, 120.0f);
    }
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _musicLibraryCollectionView) {
        if (indexPath.section == MMMusicLibrarySectionHeader) {
            MMMusicLibraryHeaderCell* headerCell = (MMMusicLibraryHeaderCell*)cell;
            headerCell.delegate = self;
        } else if (indexPath.section == MMMusicLibrarySectionAllSongsAndAssignements) {
            MMMusicLibrarySongCell* songCell = (MMMusicLibrarySongCell *)cell;
            songCell.delegate = self;
            MGSwipeButton* deleteButton = [MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor] callback:^BOOL(MGSwipeCollectionViewCell * sender){
                NSLog(@"MMMusicLibrarySongCell delete button clicked.");
                return NO;
            }];
            songCell.rightButtons = @[deleteButton];
            songCell.rightSwipeSettings.transition = MGSwipeTransitionStatic;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
//    if (_musicLibraryCollectionView == collectionView) {
//        if (indexPath.section == MMMusicLibrarySectionAllSongsAndAssignements) {
//            MMGorillaDrummerViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
//                                              instantiateViewControllerWithIdentifier:NSStringFromClass([MMGorillaDrummerViewController class])];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - MMSwipeCollectionViewCellDelegate

- (BOOL)swipeCollectionCell:(MGSwipeCollectionViewCell*) cell tappedButtonAtIndex:(NSInteger)index direction:(MGSwipeDirection)direction fromExpansion:(BOOL)fromExpansion {
    return NO;
}

#pragma mark - MMMusicLibraryHeaderCellDelegate

- (void)musicLibraryHeaderCellAskToShowAllSongs:(MMMusicLibraryHeaderCell *)cell {
    [self _showMusicLibrarySongsSection:MMMusicLibrarySongsSectionAll];
}

- (void)musicLibraryHeaderCellAskToShowAssignments:(MMMusicLibraryHeaderCell *)cell {
    [self _showMusicLibrarySongsSection:MMMusicLibrarySongsSectionAssignements];
}

#pragma mark - Actions

- (void)profileButtonAction:(id)sender {
//    UINavigationController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
//                                      instantiateViewControllerWithIdentifier:@"MMUserProfileViewControllerNavigation"];
//    [self presentViewController:vc animated:YES completion:nil];
  _musicLibraryDataSource.showRecentlyPlayedMusic = !_musicLibraryDataSource.showRecentlyPlayedMusic;
  [_musicLibraryCollectionView reloadData];
}

- (IBAction)finishSearchModeButtonAction:(id)sender {
    [self _endSearchModeAnimated:YES];
}

- (IBAction)instrumentsFilterButtonAction:(id)sender {
    [self _showInstrumentsFilterPicker];
}

- (IBAction)keyFilterButtonAction:(id)sender {
    [self _showKeysFilterPicker];
}

- (void)coinsButtonAction:(id)sender {
    MMUserStatsViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                   instantiateViewControllerWithIdentifier:@"MMUserStatsViewControllerNavigation"];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [_musicLibraryCollectionView setContentOffset:_musicLibraryCollectionView.contentOffset animated:NO];
    [_searchCollectionView setContentOffset:_searchCollectionView.contentOffset animated:NO];

    [self _beginSearchModeAnimated:YES];

    return YES;
}

- (void)_searchTextfieldDidChangeText:(UITextField *)textField {
    _searchCollectionView.hidden = (textField.text.length == 0);
    _musicLibraryCollectionView.hidden = (textField.text.length != 0);

    if (textField.text.length > 1) {
        NSString* searchString = textField.text;
        NSString* instrumentsFilterString = _instrumentsFilterPickerViewController.recentlySelectedFilterString;
        NSString* keyFilterString = _keysFilterPickerViewController.recentlySelectedFilterString;
        [_searchDataSource updateDataSourceForSearchKeyword:searchString
                                          instrumentsFilter:instrumentsFilterString
                                                 keysFilter:keyFilterString];
        [_searchCollectionView reloadData];
    }
}

#pragma mark - Private Methods

- (void)_beginSearchModeAnimated:(BOOL)animation {
    if (self.searchMode == NO) {
        self.searchMode = YES;

        _searchTextfield.placeholder = nil;

        void (^animationBlock)() = ^{
            _instrumentsFilterButton.alpha = 0.0f;
            _keyFilterButton.alpha = 0.0f;
            _doneSearchButton.alpha = 1.0f;
            _searchTextfieldContainerViewWidthConstraint.constant = 370.0f;
        };

        [UIView animateWithDuration:0.5f animations:^{
            animationBlock();
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)_endSearchModeAnimated:(BOOL)animation {
    if (self.searchMode == YES) {
        self.searchMode = NO;

        [self.view endEditing:YES];

        _searchTextfield.text = @"";
        _searchCollectionView.hidden = YES;
        _musicLibraryCollectionView.hidden = NO;
        _searchTextfield.attributedPlaceholder = [self _searchPlaceholderString];

        void (^animationBlock)() = ^{
            _instrumentsFilterButton.alpha = 1.0f;
            _keyFilterButton.alpha = 1.0f;
            _doneSearchButton.alpha = 0.0f;
            _searchTextfieldContainerViewWidthConstraint.constant = 248.0f;
        };
            
        [UIView animateWithDuration:0.5f animations:^{
            animationBlock();
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - Private Methods

- (void)_showMusicLibrarySongsSection:(MMMusicLibrarySongsSection)songsSection {
    _musicLibraryDataSource.songsSection = songsSection;
    [_musicLibraryCollectionView reloadData];
}

- (NSAttributedString *)_searchPlaceholderString {
    NSAttributedString* string = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search Songs..", nil)
                                                               attributes:@{
                                                                            NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                            NSFontAttributeName : _instrumentsFilterButton.titleLabel.font
                                                                           }];
    return string;
}

- (void)_showInstrumentsFilterPicker {
    _instrumentsFilterPickerViewController.modalPresentationStyle = UIModalPresentationPopover;
    _instrumentsFilterPickerViewController.popoverPresentationController.sourceView = _instrumentsFilterButton;
    _instrumentsFilterPickerViewController.popoverPresentationController.sourceRect = CGRectMake(20.0f, -320.0f, 200.0f, 400.0f);
    _instrumentsFilterPickerViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    [self presentViewController:_instrumentsFilterPickerViewController animated:YES completion:NULL];
}

- (void)_showKeysFilterPicker {
    _keysFilterPickerViewController.modalPresentationStyle = UIModalPresentationPopover;
    _keysFilterPickerViewController.popoverPresentationController.sourceView = _keyFilterButton;
    _keysFilterPickerViewController.popoverPresentationController.sourceRect = CGRectMake(25.0f, -320.0f, 200.0f, 400.0f);
    _keysFilterPickerViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    [self presentViewController:_keysFilterPickerViewController animated:YES completion:NULL];
}

#pragma mark - MMMusicLibrarySearchFilterPickerViewControllerDelegate

- (void)musicLibrarySearchFilterPickerViewController:(MMMusicLibrarySearchFilterPickerViewController *)filterPickerViewController
                                       didSelectItem:(NSString *)filterString {
    if (filterPickerViewController == _instrumentsFilterPickerViewController) {
        [_instrumentsFilterButton setTitle:filterString forState:UIControlStateNormal];
    } else if (filterPickerViewController == _keysFilterPickerViewController) {
        [_keyFilterButton setTitle:filterString forState:UIControlStateNormal];
        
        NSString* searchString = _searchTextfield.text;
        NSString* instrumentsFilterString = _instrumentsFilterPickerViewController.recentlySelectedFilterString;
        NSString* keyFilterString = _keysFilterPickerViewController.recentlySelectedFilterString;
        
        [_searchDataSource updateDataSourceForSearchKeyword:searchString
                                          instrumentsFilter:instrumentsFilterString
                                                 keysFilter:keyFilterString];
        [_searchCollectionView reloadData];
    }
    
    [filterPickerViewController dismissViewControllerAnimated:YES completion:nil];
}

@end

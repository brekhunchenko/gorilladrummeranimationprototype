//
//  MMStringPickerCell.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 12/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMStringPickerCell : UITableViewCell

- (void)setTextString:(NSString *)textString selected:(BOOL)selected;
@property (nonatomic, strong, readonly) NSString* textString;
@property (nonatomic, assign, readonly) BOOL stringSelected;

@end

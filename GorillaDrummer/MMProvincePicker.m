//
//  MMProvincePicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/17/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMProvincePicker.h"

@interface MMProvincePicker()  <UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation MMProvincePicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupProvinces];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupProvinces];
}

#pragma mark - Setup

- (void)_setupProvinces {
  self.dataSource = self;
  self.delegate = self;
}

#pragma mark - Class Methods

- (void)setupProvincesForCountryCode:(NSString *)countryCode {
  _countryCode = countryCode;
  
  [self reloadAllComponents];
}

#pragma mark - Custom Setters & Getters

- (void)setProvince:(MMProvince *)province {
    __block NSInteger provinceIdx = NSNotFound;
    [[self.class provincesForCountryCode:_countryCode] enumerateObjectsUsingBlock:^(MMProvince * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.province isEqualToString:province.province]) {
            provinceIdx = idx;
            *stop = YES;
        }
    }];
    _province = province;
    if (provinceIdx != NSNotFound) {
        [self selectRow:provinceIdx inComponent:0 animated:NO];
    } else {
        NSLog(@"Error: province not found.");
    }
}

#pragma mark - UIPickerView

- (NSInteger)numberOfComponentsInPickerView:(__unused UIPickerView *)pickerView {
  return 1;
}

- (NSInteger)pickerView:(__unused UIPickerView *)pickerView numberOfRowsInComponent:(__unused NSInteger)component {
  return (NSInteger)[[self class] provincesForCountryCode:self.countryCode].count;
}

- (UIView *)pickerView:(__unused UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(__unused NSInteger)component reusingView:(UIView *)view {
  if (!view) {
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
    label.backgroundColor = [UIColor clearColor];
    label.tag = 1;
    [view addSubview:label];
    
    UIImageView *flagView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
    flagView.contentMode = UIViewContentModeScaleAspectFit;
    flagView.tag = 2;
    [view addSubview:flagView];
  }
  
  ((UILabel *)[view viewWithTag:1]).text = [[[self class] provincesForCountryCode:self.countryCode][(NSUInteger)row] province];
  
  return view;
}

- (void)pickerView:(__unused UIPickerView *)pickerView
      didSelectRow:(__unused NSInteger)row
       inComponent:(__unused NSInteger)component {
  _province = [[self class] provincesForCountryCode:self.countryCode][(NSUInteger)row];
  if ([self.provincePickerDelegate respondsToSelector:@selector(provincePicker:didSelectProvince:)]) {
    [self.provincePickerDelegate provincePicker:self didSelectProvince:_province];
  }
}

#pragma mark - Class Methods

+ (NSArray<NSString *> *)provincesForCountryCode:(NSString *)countryCode {
  static NSArray* provincesInfo = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"provinces" ofType:@"json"];
    NSError * error = nil;
    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    NSData * jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];

    provincesInfo = parsedData;
  });
  
  NSMutableArray* provincesMutable = [NSMutableArray new];
  for (NSDictionary* dict in provincesInfo) {
    if ([[dict objectForKey:@"country"] isEqualToString:countryCode]) {
      MMProvince* province = [[MMProvince alloc] initWithDictionary:dict];
      [provincesMutable addObject:province];
    }
  }
    
  return provincesMutable;
}

+ (MMProvince *)provinceForProvinceShort:(NSString *)provinceShort countryCode:(NSString *)countryCode {
  __block NSInteger provinceIdx = NSNotFound;
  [[self.class provincesForCountryCode:countryCode] enumerateObjectsUsingBlock:^(MMProvince * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    if ([obj.provinceShort isEqualToString:provinceShort]) {
      provinceIdx = idx;
      *stop = YES;
    }
  }];
  
  if (provinceIdx != NSNotFound) {
    return [[self provincesForCountryCode:countryCode] objectAtIndex:provinceIdx];
  } else {
    return nil;
  }
}

@end

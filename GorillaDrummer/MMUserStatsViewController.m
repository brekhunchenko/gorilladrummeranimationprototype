//
//  MMUserStatsViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/25/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMUserStatsViewController.h"
#import "MMClassStandingCell.h"

@interface MMUserStatsViewController () < UITableViewDelegate, UITableViewDataSource >

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *rantingDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *statsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfCoinsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfPitchBadgesLabel;
@property (weak, nonatomic) IBOutlet UITableView *classStandingTableView;

@end

@implementation MMUserStatsViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"Your Stats";
  
  [self _setupLabels];
  [self _setupProfileAvatar];
  [self _setupBarButtonItems];
  [self _setupTableView];
}

#pragma mark - Setup

- (void)_setupTableView {
  _classStandingTableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 41.0f, 0.0f);
}

- (void)_setupLabels {
  _rantingDateLabel.text = [NSString stringWithFormat:@""];
  _positionLabel.text = [NSString stringWithFormat:@"#%d", (int)3];
  NSInteger numberOfCoins = 353;
  NSInteger numberOfPitchBadges = 14;
  _statsLabel.text = [NSString stringWithFormat:@"You earned %lu coins and %lu pitch badges this week! Way to go.", numberOfCoins, numberOfPitchBadges];
  _numberOfCoinsLabel.text = [NSString stringWithFormat:@"%lu", numberOfCoins];
  _numberOfPitchBadgesLabel.text = [NSString stringWithFormat:@"%lu", numberOfPitchBadges];
}

- (void)_setupProfileAvatar {
  _avatarImageView.layer.cornerRadius = 86.0f;
  _avatarImageView.layer.masksToBounds = YES;
  _avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)_setupBarButtonItems {
  UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(doneButtonAction:)];
  doneButton.tintColor = [UIColor whiteColor];
  [doneButton setTitleTextAttributes:@{
                                       NSFontAttributeName : [UIFont systemFontOfSize:20.0f]
                                       }
                            forState:UIControlStateNormal];
  self.navigationItem.rightBarButtonItem = doneButton;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  MMClassStandingCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MMClassStandingCell class])];
  return cell;
}

#pragma mark - Actions

- (void)doneButtonAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end

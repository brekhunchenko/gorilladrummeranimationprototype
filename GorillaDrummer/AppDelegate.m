//
//  AppDelegate.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "AppDelegate.h"
#import "MMNavigationViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - UIApplication

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Auth" bundle:nil]
                                    instantiateViewControllerWithIdentifier:@"SplashScreen"];
  [self.window makeKeyAndVisible];
  
  CGFloat splashScreenDelay = 1.5f;
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(splashScreenDelay * NSEC_PER_SEC)),
                 dispatch_get_main_queue(), ^{
    BOOL userLoggedIn = NO;
    if (userLoggedIn == NO) {
      UINavigationController* singUpNavigation = [[UIStoryboard storyboardWithName:@"Auth" bundle:nil]
                                                  instantiateViewControllerWithIdentifier:@"MMLoginViewControllerNavigation"];
      singUpNavigation.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
      singUpNavigation.modalPresentationStyle = UIModalPresentationOverFullScreen;
      self.window.rootViewController = singUpNavigation;
    } else {
      MMNavigationViewController* navController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                   instantiateViewControllerWithIdentifier:@"MMMusicLibraryViewControllerNavigation"];
      navController.modalPresentationStyle = UIModalPresentationFullScreen;
      self.window.rootViewController = navController;
    }
  });
    
  return YES;
}

@end

//
//  MMRecentlyPlayedCell.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMRecentlyPlayedCell.h"

@interface MMRecentlyPlayedCell()

@property (weak, nonatomic) IBOutlet UIImageView *difficultyImageView;
@property (weak, nonatomic) IBOutlet UILabel *arrangementNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfCoinsLabel;

@end

@implementation MMRecentlyPlayedCell

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
}

- (void)prepareForReuse {
  [super prepareForReuse];
  
  //TEMP
  _arrangementNameLabel.text = (rand()%2 == 0 ? @"Eensy Weensy Spider" : @"Canon in D");
  _detailsLabel.text = @"Trumpet - D";
}

#pragma mark - Custom Setters & Getters

- (void)setSongDifficulty:(SongDifficulty)songDifficulty {
  _songDifficulty = songDifficulty;
  
  UIImage* difficultyImage = nil;
  switch (songDifficulty) {
    case SongDifficultyEasy: {
      difficultyImage = [UIImage imageNamed:@"Card-Easy"];
    }
      break;
    case SongDifficultyModerate: {
      difficultyImage = [UIImage imageNamed:@"Card-Moderate"];
    }
      break;
    case  SongDifficultyBeginner: {
      difficultyImage = [UIImage imageNamed:@"Card-Basic"];
    }
      break;
    case SongDifficultyAdvanced: {
      difficultyImage = [UIImage imageNamed:@"Card-Hard"];
    }
      break;
  }
  _difficultyImageView.image = difficultyImage;
}

@end

//
//  MMMusicLibrarySearchDataSource.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMMusicLibrarySearchDataSource.h"
#import "MMSearchResultCell.h"

@implementation MMMusicLibrarySearchDataSource

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    _listOfInstrumentsAvailable = @[
                                    @"All",
                                    @"Piccolo",
                                    @"Flute",
                                    @"Clarinet",
                                    @"A Clarinet",
                                    @"Soprano Saxophone",
                                    @"Alto Saxophone",
                                    @"Tenor Saxophone",
                                    @"Baritone Saxophone",
                                    @"Bass Saxophone",
                                    @"Trumpet",
                                    @"Cornet",
                                    @"Oboe",
                                    @"French Horn",
                                    @"Bassoon",
                                    @"Trombone",
                                    @"Baritone Horn",
                                    @"Euphonium",
                                    @"Tuba",
                                    @"Piano",
                                    @"Guitar",
                                    @"Percussion",
                                    @"Vocal",
                                    @"Violin",
                                    @"Viola",
                                    @"Cello",
                                    @"Bass",
                                    @"Woodwind",
                                    @"Brass",
                                    @"Wind",
                                    @"Strings",
                                   ];
      
      _listOfKeysAvailable = @[
                               @"C Major",
                               @"C# Major",
                               @"D Major",
                               @"Eb Major",
                               @"E Major",
                               @"F Major",
                               @"F# Major",
                               @"G Major",
                               @"Ab Major",
                               @"A Major",
                               @"Bb Major",
                               @"B Major",
                               @"A# minor",
                               @"D# minor",
                               @"G# minor",
                               @"C# minor",
                               @"F# minor",
                               @"B minor",
                               @"E minor",
                               @"A minor",
                               @"D minor",
                               @"G minor",
                               @"C minor",
                               @"F minor",
                               @"Bb minor",
                               @"Eb minor",
                               @"Ab minor"
                              ];
  }
  return self;
}

#pragma mark - Class Methods

- (void)updateDataSourceForSearchKeyword:(NSString *)searchKeyword
                       instrumentsFilter:(NSString *)instrumentsFilter
                              keysFilter:(NSString *)keysFilter {
    if (searchKeyword.length > 0) {
        if (instrumentsFilter.length > 0) {
            //TODO: Add instruments predicate.
        }
        if (keysFilter.length > 0) {
            //TODO: Add keys predicate.
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  NSInteger numberOfItems = 100;
  return numberOfItems;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MMSearchResultCell* headerCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MMSearchResultCell class])
                                                                             forIndexPath:indexPath];
  return headerCell;
}

@end

//
//  MMGradientView.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/20/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMGradientView : UIView

- (void)updateGradientWithTopColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor;

@property (nonatomic, strong, readwrite) UIColor *topColor;
@property (nonatomic, strong, readwrite) UIColor *bottomColor;

@end

//
//  LocationManager.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern NSString * const kLocationManagerDidChangeLocationNotification;

@interface MMLocationManager : NSObject

@property (nonatomic, strong, readonly) CLLocation* userLocation;

+ (instancetype)sharedManager;

- (void)startLocationUpdates;
- (void)refreshLocation;

@end

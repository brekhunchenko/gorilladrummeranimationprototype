//
//  MMProvince.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/27/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMProvince : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@property (nonatomic, strong, readonly) NSString* countryCode;
@property (nonatomic, strong, readonly) NSString* province;
@property (nonatomic, strong, readonly) NSString* provinceShort;

@end

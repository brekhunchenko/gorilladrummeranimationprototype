//
//  MMRoundedButton.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/24/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMRoundedButton.h"

@implementation MMRoundedButton

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _initialSetup];
}

#pragma mark - Setup

- (void)_initialSetup {
  self.layer.cornerRadius = 30.0f;
  self.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6f].CGColor;
  self.layer.borderWidth = 1.5f;
    
  self.titleLabel.textAlignment = NSTextAlignmentCenter;
  self.titleLabel.adjustsFontSizeToFitWidth = YES;
  self.titleLabel.minimumScaleFactor = 0.4f;
}

#pragma mark - Overridden

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.titleLabel.frame = CGRectInset(self.bounds, 8.0f, 0.0f);
}

@end

//
//  MMSearchResultCell.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMSearchResultCell.h"

@interface MMSearchResultCell()

@property (weak, nonatomic) IBOutlet UIImageView *instrumentIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *arrangementNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

@end

@implementation MMSearchResultCell

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupInstrumentIconImageView];
}

#pragma mark - Setup

- (void)_setupInstrumentIconImageView {
  [self layoutIfNeeded];
  
  _instrumentIconImageView.layer.cornerRadius = _instrumentIconImageView.bounds.size.height/2.0f;
  _instrumentIconImageView.layer.masksToBounds = YES;
}

@end

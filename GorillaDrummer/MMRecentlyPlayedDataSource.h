//
//  MMRecentlyPlayedDataSource.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MMRecentlyPlayedDataSource : NSObject < UICollectionViewDataSource >

@end

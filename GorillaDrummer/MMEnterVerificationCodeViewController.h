//
//  MMEnterVerificationCodeViewController.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 1/7/18.
//  Copyright © 2018 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MMEnterVerificationCodeViewControllerDelegate;

@interface MMEnterVerificationCodeViewController : UIViewController

//Returns empty string if not all digits entered.
- (NSString *)verificationCode;

@property (nonatomic, weak) id<MMEnterVerificationCodeViewControllerDelegate> delegate;

@end

@protocol MMEnterVerificationCodeViewControllerDelegate < NSObject >

- (void)enterVerificationCodeViewController:(MMEnterVerificationCodeViewController *)vc didFinishEnteringCode:(NSString *)verificationCode;

@end


//
//  MMMusicLibraryHeaderCell.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMMusicLibraryHeaderCell.h"
#import "MMRecentlyPlayedCell.h"
#import "MMRecentlyPlayedDataSource.h"

@interface MMMusicLibraryHeaderCell() < UICollectionViewDelegate >

@property (weak, nonatomic) IBOutlet UILabel *recentlyPlayedLabel;
@property (nonatomic, strong) MMRecentlyPlayedDataSource* recentlyPlayedDataSource;
@property (weak, nonatomic) IBOutlet UICollectionView *recentlyPlayedCollectionView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *songsSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *numberOfAssignmentsAvailableLabel;
@property (weak, nonatomic) IBOutlet UIView *numberOfAssignmentsAvailableContainerView;
@end

@implementation MMMusicLibraryHeaderCell

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  _showRecentlyPlayed = NO;
  
  [self _setupSegmenetedControl];
  [self _setupCollectionView];
  [self _setupLabels];
}

#pragma mark - Setup

- (void)_setupSegmenetedControl {
  [_songsSegmentedControl setTitleTextAttributes:@{
                                                   NSFontAttributeName : [UIFont systemFontOfSize:15.0f],
                                                   NSForegroundColorAttributeName : [UIColor whiteColor]
                                                  }
                                        forState:UIControlStateNormal];
  [_songsSegmentedControl setTitleTextAttributes:@{
                                                   NSFontAttributeName : [UIFont systemFontOfSize:15.0f],
                                                   NSForegroundColorAttributeName : [UIColor whiteColor]
                                                   }
                                        forState:UIControlStateSelected];
}

- (void)_setupCollectionView {
  _recentlyPlayedDataSource = [[MMRecentlyPlayedDataSource alloc] init];
  _recentlyPlayedCollectionView.dataSource = _recentlyPlayedDataSource;
  _recentlyPlayedCollectionView.delegate = self;
}

- (void)_setupLabels {
  _numberOfAssignmentsAvailableContainerView.layer.cornerRadius = 13.0f;
  _numberOfAssignmentsAvailableContainerView.layer.masksToBounds = YES;
  [_numberOfAssignmentsAvailableContainerView layoutIfNeeded];
}

#pragma mark - Custom Setters & Getters 

- (void)setNumberOfAvailableAssignments:(NSInteger)numberOfAvailableAssignments {
  _numberOfAvailableAssignments = numberOfAvailableAssignments;
  
  _numberOfAssignmentsAvailableContainerView.hidden = (_numberOfAvailableAssignments == 0);
  _numberOfAssignmentsAvailableLabel.text = [NSString stringWithFormat:@"%lu", numberOfAvailableAssignments];
}

- (void)setShowRecentlyPlayed:(BOOL)showRecentlyPlayed {
  _showRecentlyPlayed = showRecentlyPlayed;
  
  _recentlyPlayedCollectionView.hidden = !_showRecentlyPlayed;
  _recentlyPlayedLabel.hidden = !_showRecentlyPlayed;
  
}
#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  //TODO:
}

#pragma mark - Actions

- (IBAction)songsSegmentedControlValueChanged:(UISegmentedControl *)sender {
  if (sender.selectedSegmentIndex == 0) {
    if ([self.delegate respondsToSelector:@selector(musicLibraryHeaderCellAskToShowAssignments:)]) {
      [self.delegate musicLibraryHeaderCellAskToShowAssignments:self];
    }
  } else if (sender.selectedSegmentIndex == 1) {
    if ([self.delegate respondsToSelector:@selector(musicLibraryHeaderCellAskToShowAllSongs:)]) {
      [self.delegate musicLibraryHeaderCellAskToShowAllSongs:self];
    }
  }
}

#pragma mark - Private Methods

- (void)_setNumberOfAssignmentsAvailable:(NSInteger)numberOfAssignments {
  _numberOfAssignmentsAvailableLabel.text = [NSString stringWithFormat:@"%d", (int)numberOfAssignments];
}

#pragma mark - Class Methods

+ (CGFloat)cellHeight:(BOOL)showRecentlyPlayed {
  if (showRecentlyPlayed) {
    return 399.0f;
  } else {
    return 100.0f;
  }
}

@end

//
//  MMUserProfileViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/24/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMUserProfileViewController.h"

@interface MMUserProfileViewController () < UIImagePickerControllerDelegate, UINavigationControllerDelegate >

@property (weak, nonatomic) IBOutlet UILabel *userIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *roleLabel;
@property (weak, nonatomic) IBOutlet UILabel *designedByAndVersionNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileAvatarImageView;

@end

@implementation MMUserProfileViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"About";
  
  [self _setupLabels];
  [self _setupBarButtonItems];
  [self _setupProfileAvatar];
}

#pragma mark - Setup

- (void)_setupBarButtonItems {
  UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(doneButtonAction:)];
  doneButton.tintColor = [UIColor whiteColor];
  [doneButton setTitleTextAttributes:@{
                                       NSFontAttributeName : [UIFont systemFontOfSize:20.0f]
                                      }
                            forState:UIControlStateNormal];
  self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)_setupLabels {
  _designedByAndVersionNumberLabel.text = [NSString stringWithFormat:@"Windband was designed by Meta Music Inc. \n Version %@, Build %@", [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"], [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]];
}

- (void)_setupProfileAvatar {
  _profileAvatarImageView.layer.cornerRadius = 68.0f;
  _profileAvatarImageView.layer.masksToBounds = YES;
  _profileAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
}

#pragma mark - Actions

- (IBAction)profileAvatarButtonAction:(id)sender {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Choose a Source" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
  
  UIAlertAction* takePhoto = [UIAlertAction actionWithTitle:@"Take a photo"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                      
                                                    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                                      UIImagePickerController* picker = [[UIImagePickerController alloc] init];
                                                      picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                      picker.delegate = self;
                                                      picker.modalPresentationStyle = UIModalPresentationPopover;
                                                      picker.popoverPresentationController.sourceView = sender;
                                                      picker.popoverPresentationController.sourceRect = self.view.bounds;
                                                      [self presentViewController:picker animated:YES completion:NULL];
                                                    }
                                                  }];
  UIAlertAction* choosePicture = [UIAlertAction actionWithTitle:@"Choose from gallery"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                          
                                                        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
                                                        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                        picker.delegate = self;
                                                        picker.modalPresentationStyle = UIModalPresentationPopover;
                                                        picker.popoverPresentationController.sourceView = sender;
                                                        picker.popoverPresentationController.sourceRect = CGRectMake(-170.0f, -250.0f, 320.0f, 640.0f);
                                                        picker.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
                                                        [self presentViewController:picker animated:YES completion:NULL];
                                                      }];
  
  UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleCancel
                                                 handler:nil];
  
  [alertController addAction:takePhoto];
  [alertController addAction:choosePicture];
  [alertController addAction:cancel];
  
  alertController.popoverPresentationController.sourceView = sender;
  alertController.popoverPresentationController.sourceRect = CGRectMake(-170.0f, -250.0f, 320.0f, 640.0f);
  alertController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
  
  [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)metamusicWebSiteButtonAction:(id)sender {
  NSURL* metamusicURL = [NSURL URLWithString:@"http://metamusictech.com/"];
  [[UIApplication sharedApplication] openURL:metamusicURL
                                     options:@{}
                           completionHandler:nil];
}

- (void)doneButtonAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
  __unused UIImage *image = info[UIImagePickerControllerOriginalImage];
  //TODO: Save image.
  _profileAvatarImageView.image = image;
  [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [picker dismissViewControllerAnimated:YES completion:nil];
}

@end

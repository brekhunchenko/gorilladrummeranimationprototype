//
//  MMTextureManager.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMTextureManager.h"

static NSString* const kConfettiAnimationTexturesKey = @"kConfettiAnimationTexturesKey";

@interface MMTextureManager()

@property (nonatomic ,strong) NSMutableDictionary* cachedTextures;

@end

@implementation MMTextureManager

#pragma mark - Class Methods

+ (instancetype)sharedInstance {
  static MMTextureManager* manager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    manager = [[MMTextureManager alloc] init];
  });
  return manager;
}

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    _cachedTextures = [NSMutableDictionary new];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_cleanCachedTextures)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification
                                               object:nil];
  }
  return self;
}

#pragma mark - Public Methods

- (void)preloadTextures {
  [self _preloadGorillaExpressionsTextures];
  [self _preloadGorillaBeatTipesTextures];
  [self _preloadConfettiAnimationTextures];
}

- (NSArray<SKTexture *> *)gorillaFaceTexturesForExpression:(GorillaDrummerExpression)expression {
  NSArray<SKTexture *> *cachedTextures = [self _getCachedTexturesForExpression:expression];
  if (cachedTextures) {
    return cachedTextures;
  }
  
  NSMutableArray<SKTexture *> *texturesMutable = [NSMutableArray new];
  NSInteger numberOfTextures = 0;
  switch (expression) {
    case GorillaDrummerExpressionNeutral: numberOfTextures = 1; break;
    case GorillaDrummerExpressionDisappointed: numberOfTextures = 10; break;
    case GorillaDrummerExpressionHappy: numberOfTextures = 5; break;
  }
  
  for (int i = 0; i < numberOfTextures; i++) {
    NSString* textureName = [self _textureNameForExpression:expression
                                               textureIndex:i];
    SKTexture* texture = [SKTexture textureWithImageNamed:textureName];
    NSParameterAssert(texture);
    [texturesMutable addObject:texture];
  }
  NSArray<SKTexture *> *textures = [NSArray arrayWithArray:texturesMutable];
  return textures;
}

- (NSArray<SKTexture *> *)gorillaBeatAnimationTexturesForType:(GorillaDrummerBeatType)beatType {
  NSArray<SKTexture *> *cachedTextures = [self _getCachedTexturesForBeatType:beatType];
  if (cachedTextures) {
    return cachedTextures;
  }
  
  NSMutableArray<SKTexture *> *texturesMutable = [NSMutableArray new];
  NSInteger numberOfTextures = 1;
  switch (beatType) {
    case GorillaDrummerBeatType_0: numberOfTextures = 17; break;
    case GorillaDrummerBeatType_1: numberOfTextures = 16; break;
    case GorillaDrummerBeatType_2: numberOfTextures = 16; break;
    case GorillaDrummerBeatType_3: numberOfTextures = 16; break;
    case GorillaDrummerBeatType_Small0: numberOfTextures = 17; break;
    case GorillaDrummerBeatType_Small1: numberOfTextures = 16; break;
  }
  
  for (int i = 0; i < numberOfTextures; i++) {
    NSString* textureName = [self _textureNameForBeatType:beatType
                                             textureIndex:i];
    SKTexture* texture = [SKTexture textureWithImageNamed:textureName];
    NSParameterAssert(texture);
    [texturesMutable addObject:texture];
  }
  NSArray<SKTexture *> *textures = [NSArray arrayWithArray:texturesMutable];
  return textures;
}

- (NSArray<SKTexture *> *)confettiAnimation {
  NSArray<SKTexture *> *cachedTextures = [_cachedTextures objectForKey:kConfettiAnimationTexturesKey];
  if (cachedTextures) {
    return cachedTextures;
  } else {
    [self _preloadConfettiAnimationTextures];
    return [_cachedTextures objectForKey:kConfettiAnimationTexturesKey];
  }
}

#pragma mark - Private Methods

- (void)_preloadGorillaExpressionsTextures {
  NSArray* expressionsList = @[
                               @(GorillaDrummerExpressionNeutral),
                               @(GorillaDrummerExpressionHappy),
                               @(GorillaDrummerExpressionDisappointed)
                              ];
  
  for (NSNumber* expressionNumber in expressionsList) {
    GorillaDrummerExpression expression = expressionNumber.integerValue;
    NSArray* textures = [self gorillaFaceTexturesForExpression:expression];
    [self _cacheTextures:textures
           forExpression:expression];
  }
}

- (void)_preloadGorillaBeatTipesTextures {
  NSArray* beatTypesList = @[
                             @(GorillaDrummerBeatType_0),
                             @(GorillaDrummerBeatType_1),
                             @(GorillaDrummerBeatType_2),
                             @(GorillaDrummerBeatType_3),
                             @(GorillaDrummerBeatType_Small0),
                             @(GorillaDrummerBeatType_Small1)
                            ];
  
  for (NSNumber* beatTypeNumber in beatTypesList) {
    GorillaDrummerBeatType beatType = beatTypeNumber.integerValue;
    NSArray* textures = [self gorillaBeatAnimationTexturesForType:beatType];
    [self _cacheTextures:textures forBeatType:beatType];
  }
}

- (void)_preloadConfettiAnimationTextures {
  NSMutableArray* texturesMutable = [NSMutableArray new];
  for (int i = 0; i <= 20; i++) {
    NSString* textureName = [NSString stringWithFormat:@"Confetti%04d", i];
    SKTexture* texture = [SKTexture textureWithImageNamed:textureName];
    [texturesMutable addObject:texture];
  }
  [_cachedTextures setObject:texturesMutable
                      forKey:kConfettiAnimationTexturesKey];
}

- (NSArray <SKTexture *> *)_getCachedTexturesForBeatType:(GorillaDrummerBeatType)beatType {
  NSString* hashString = [self _hashStringForBeatType:beatType];
  return [_cachedTextures objectForKey:hashString];
}

- (NSArray <SKTexture *> *)_getCachedTexturesForExpression:(GorillaDrummerExpression)expression {
  NSString* hashString = [self _hashStringForExpression:expression];
  return [_cachedTextures objectForKey:hashString];
}

- (void)_cacheTextures:(NSArray<SKTexture *> *)textures
         forExpression:(GorillaDrummerExpression)expression {
  NSString* hashString = [self _hashStringForExpression:expression];
  [_cachedTextures setObject:textures
                      forKey:hashString];
}

- (void)_cacheTextures:(NSArray<SKTexture *> *)textures
           forBeatType:(GorillaDrummerBeatType)beatType {
  NSString* hashString = [self _hashStringForBeatType:beatType];
  [_cachedTextures setObject:textures
                      forKey:hashString];
}

- (NSString *)_hashStringForExpression:(GorillaDrummerExpression)expression {
  NSString* hashString = @"";
  switch (expression) {
    case GorillaDrummerExpressionHappy: hashString = @"happy"; break;
    case GorillaDrummerExpressionNeutral: hashString = @"neutral"; break;
    case GorillaDrummerExpressionDisappointed: hashString = @"GorillaHead"; break;
  }
  return hashString;
}

- (NSString *)_hashStringForBeatType:(GorillaDrummerBeatType)beatType {
  return [NSString stringWithFormat:@"beat_type_%d", (int)beatType];
}

- (NSString *)_textureNameForExpression:(GorillaDrummerExpression)expression
                           textureIndex:(NSInteger)textureIndex {
  NSInteger textureOffset = 0;
  switch (expression) {
    case GorillaDrummerExpressionNeutral: textureOffset = 0; break;
    case GorillaDrummerExpressionDisappointed: textureOffset = 15; break;
    case GorillaDrummerExpressionHappy: textureOffset = 0; break;
  }
  NSString* prefix = @"GorillaHead";
  NSString* textureName = [NSString stringWithFormat:@"%@%04d", prefix, (int)(textureIndex + textureOffset)];
  return textureName;
}

- (NSString *)_textureNameForBeatType:(GorillaDrummerBeatType)expression
                         textureIndex:(NSInteger)textureIndex {
  NSInteger textureOffset = 0;
  switch (expression) {
    case GorillaDrummerBeatType_0: textureOffset = 0; break;
    case GorillaDrummerBeatType_1: textureOffset = 17; break;
    case GorillaDrummerBeatType_2: textureOffset = 33; break;
    case GorillaDrummerBeatType_3: textureOffset = 49; break;
    case GorillaDrummerBeatType_Small0: textureOffset = 80; break;
    case GorillaDrummerBeatType_Small1: textureOffset = 97; break;
  }
  NSString* prefix = @"GorillaAnimation";
  NSString* textureName = [NSString stringWithFormat:@"%@%04d", prefix, (int)(textureIndex + textureOffset)];
  return textureName;
}

#pragma mark - Dealloc

- (void)_cleanCachedTextures {
  [_cachedTextures removeAllObjects];
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

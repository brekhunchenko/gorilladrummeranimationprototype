//
//  MMPhoneCodePicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCountryPhoneCode.h"

@protocol MMPhoneCodePickerDelegate;

@interface MMPhoneCodePicker : UIPickerView

@property (nonatomic, weak) id<MMPhoneCodePickerDelegate> phoneCodePickerDelegate;

@property (nonatomic, strong) MMCountryPhoneCode* selectedPhoneCode;

+ (NSArray <MMCountryPhoneCode *> *)listOfCountryPhoneCodes;
+ (MMCountryPhoneCode *)phoneCodeForCountryCode:(NSString *)countryCode;

@end

@protocol MMPhoneCodePickerDelegate <NSObject>

- (void)phoneCodePicker:(MMPhoneCodePicker *)picker didSelectPhoneCode:(MMCountryPhoneCode *)phoneCode;

@end

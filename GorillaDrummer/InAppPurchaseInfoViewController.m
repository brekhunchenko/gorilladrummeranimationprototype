//
//  TermsOfUseViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 9/13/18.
//  Copyright © 2018 Yalex Mobile. All rights reserved.
//

#import "InAppPurchaseInfoViewController.h"
#import "MMRoundedButton.h"

@interface InAppPurchaseInfoViewController ()

@property (weak, nonatomic) IBOutlet MMRoundedButton *monthlySubscriptionButton;
@property (weak, nonatomic) IBOutlet MMRoundedButton *annualSubscriptionButton;
@property (weak, nonatomic) IBOutlet MMRoundedButton *restoreButton;
@property (weak, nonatomic) IBOutlet MMRoundedButton *continueButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;

@property (nonatomic, assign, readwrite) TermsOfUseLanguage language;

@end

@implementation InAppPurchaseInfoViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupButtons];
    [self _setupTextView];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Setup

- (void)_setupButtons {
    NSString* monthlySubscriptionButtonTitle = @"";
    if (self.language == TermsOfUseLanguageEnglish) {
        monthlySubscriptionButtonTitle = [NSString stringWithFormat:@"monthly \n\n %@/ month", [self _monthlyPriceString]];
    } else {
        monthlySubscriptionButtonTitle = [NSString stringWithFormat:@"monthly \n\n %@/ month", [self _monthlyPriceString]];
    }
    _monthlySubscriptionButton.titleLabel.numberOfLines = 4;
    _monthlySubscriptionButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_monthlySubscriptionButton setTitle:monthlySubscriptionButtonTitle forState:UIControlStateNormal];
    
    NSString* annualSubscriptionButtonTitle = @"";
    if (self.language == TermsOfUseLanguageEnglish) {
        annualSubscriptionButtonTitle = [NSString stringWithFormat:@"annual \n\n %@/ month", [self _annualPriceString]];
    } else {
        annualSubscriptionButtonTitle = [NSString stringWithFormat:@"annual \n\n %@/ month", [self _annualPriceString]];
    }
    _annualSubscriptionButton.titleLabel.numberOfLines = 4;
    _annualSubscriptionButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_annualSubscriptionButton setTitle:annualSubscriptionButtonTitle forState:UIControlStateNormal];
    
    NSString* restoreButtonTitle = @"";
    if (self.language == TermsOfUseLanguageEnglish) {
        restoreButtonTitle = [NSString stringWithFormat:@"restore \nsubscription \niOS"];
    } else {
        restoreButtonTitle = [NSString stringWithFormat:@"restore \nsubscription \niOS"];
    }
    _restoreButton.titleLabel.numberOfLines = 3;
    _restoreButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];
    
    [self.languageButton setTitle:(self.language == TermsOfUseLanguageEnglish ? @"EN" : @"CH") forState:UIControlStateNormal];
}

- (void)_setupTextView {
    self.language = TermsOfUseLanguageEnglish;
}

#pragma mark - Custom Setters & Getters

- (void)setLanguage:(TermsOfUseLanguage)language {
    _language = language;
    
    NSString *txtFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"terms_of_use_%@", (language == TermsOfUseLanguageEnglish ? @"en" : @"ch")] ofType:@"txt"];
    NSString *termsOfUseString = [NSString stringWithContentsOfFile:txtFilePath encoding:NSUTF8StringEncoding error:NULL];
    termsOfUseString = [termsOfUseString stringByReplacingOccurrencesOfString:@"*MONTHLY_PRICE*" withString:[self _monthlyPriceString]];
    termsOfUseString = [termsOfUseString stringByReplacingOccurrencesOfString:@"*ANNUAL_PRICE*" withString:[self _annualPriceString]];
    self.textView.text = termsOfUseString;
    
    [self _setupButtons];
}

#pragma mark - Actions

- (IBAction)languageButtonAction:(id)sender {
    self.language = (self.language == TermsOfUseLanguageEnglish ? TermsOfUseLanguageChinese : TermsOfUseLanguageEnglish);
}

- (IBAction)termsOfUseButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(inAppPurchaseInfoViewControllerDidAskToShowTermsOfUse:)]) {
        [self.delegate inAppPurchaseInfoViewControllerDidAskToShowTermsOfUse:self];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)privacyPolicyButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(inAppPurchaseInfoViewControllerDidAskToShowPrivacyPolicy:)]) {
        [self.delegate inAppPurchaseInfoViewControllerDidAskToShowPrivacyPolicy:self];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)newMonthlySubscriptionButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(inAppPurchaseInfoViewControllerDidAskToShowNewMonthlySubscription:)]) {
        [self.delegate inAppPurchaseInfoViewControllerDidAskToShowNewMonthlySubscription:self];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)nexAnnualSubscriptionButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(inAppPurchaseInfoViewControllerDidAskToShowAnnualSubscription:)]) {
        [self.delegate inAppPurchaseInfoViewControllerDidAskToShowNewMonthlySubscription:self];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)restoreSubscriptionButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(inAppPurchaseInfoViewControllerDidAskToRestorePurchases:)]) {
        [self.delegate inAppPurchaseInfoViewControllerDidAskToRestorePurchases:self];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)continueAsAGuestButtonAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(continueAsAGuestButtonAction:)]) {
        [self.delegate inAppPurchaseInfoViewControllerDidAskToContinueAsGuest:self];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark - Utilz

- (NSString *)_monthlyPriceString {
    return @"$2.99"; // Should be getted from in-app purchase
}

- (NSString *)_annualPriceString {
    return @"$299.99"; // Should be getted from in-app purchase
}

@end

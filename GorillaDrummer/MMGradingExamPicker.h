//
//  MMGradingExamPicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/31/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMStgringPickerController.h"

@protocol MMGradingExamPickerDelegate;

@interface MMGradingExamPicker : MMStgringPickerController

@property (nonatomic, weak) id<MMGradingExamPickerDelegate> gradingExamDelegate;
@property (nonatomic, strong) NSString* gradingExam;

+ (NSArray <NSString *> *)listOfGradingExams;

@end

@protocol MMGradingExamPickerDelegate <NSObject>

- (void)gradingExamPicker:(MMGradingExamPicker *)picker didSelectGradingExam:(NSString *)gradingExam;

@end

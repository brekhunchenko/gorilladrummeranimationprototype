//
//  MMCountryPhoneCode.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMCountryPhoneCode : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@property (nonatomic, strong, readonly) NSString* countryCode;
@property (nonatomic, strong, readonly) NSString* phoneCode;

@end

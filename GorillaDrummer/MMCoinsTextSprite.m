//
//  MMCoinsTextSprite.m
//  GorillaDrummer
//
//  Created by Yaroslav on 11/1/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMCoinsTextSprite.h"

@interface MMCoinsTextSprite()

@property (nonatomic, strong) SKLabelNode* labelNode0;
@property (nonatomic, strong) SKLabelNode* labelNode1;
@property (nonatomic, strong) SKSpriteNode* coinsSpriteImageNode;

@end

@implementation MMCoinsTextSprite

#pragma mark - Initializers

- (instancetype)init {
  self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(1024.0f, 100.0f)];
  if (self) {
    _labelNode0 = [[SKLabelNode alloc] initWithFontNamed:@"Helvetica-Regular"];
    _labelNode0.fontSize = 30.0f;
    _labelNode0.fontColor = [UIColor whiteColor];
    _labelNode0.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _labelNode0.position = CGPointMake(self.size.width/2.0f, self.size.height/2.0f);
    _labelNode0.zPosition = 10;
    [self addChild:_labelNode0];

    _labelNode1 = [[SKLabelNode alloc] initWithFontNamed:@"Helvetica-Regular"];
    _labelNode1.fontSize = 30.0f;
    _labelNode1.fontColor = [UIColor whiteColor];
    _labelNode1.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _labelNode1.position = CGPointMake(self.size.width/2.0f, self.size.height/2.0f);
    _labelNode1.zPosition = 10;
    [self addChild:_labelNode1];
    
    _coinsSpriteImageNode = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor]
                                                       size:CGSizeMake(111.0f, 120.0f)];
    _coinsSpriteImageNode.texture = [SKTexture textureWithImageNamed:@"CoinsEarned"];
    [self addChild:_coinsSpriteImageNode];
  }
  return self;
}

#pragma mark - Class Methods

- (void)setText:(NSString *)text numberOfCoins:(NSInteger)numberOfCoins {
  _labelNode0.text = text;
  _labelNode0.position = CGPointMake(-_labelNode0.frame.size.width/2.0f - _coinsSpriteImageNode.size.width/2.0f - 4.0f,
                                     0.0f);
  
  _coinsSpriteImageNode.position = CGPointMake(0.0f, 0.0f);

  _labelNode1.text = [NSString stringWithFormat:@"%d coins!", (int)numberOfCoins];
  _labelNode1.position = CGPointMake(_labelNode1.frame.size.width/2.0f + _coinsSpriteImageNode.size.width/2.0f + 4.0f,
                                     0.0f);
  
  CGFloat centerOffset = ((_labelNode1.frame.size.width + _coinsSpriteImageNode.size.width/2.0f + 4.0f) - (_labelNode0.frame.size.width + _coinsSpriteImageNode.size.width/2.0f + 4.0f))/2.0f;
  _labelNode0.position = CGPointMake(_labelNode0.position.x - centerOffset,
                                     _labelNode0.position.y);
  _coinsSpriteImageNode.position = CGPointMake(_coinsSpriteImageNode.position.x - centerOffset,
                                               _coinsSpriteImageNode.position.y);
  _labelNode1.position = CGPointMake(_labelNode1.position.x - centerOffset,
                                     _labelNode1.position.y);
  
}

@end

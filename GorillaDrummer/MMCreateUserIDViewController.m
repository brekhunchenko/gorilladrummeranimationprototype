//
//  MMCreateUserIDViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 12/24/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMCreateUserIDViewController.h"
#import "MMSignUpTextField.h"

#import "MMCreateAccountViewController.h"
#import "MMRoundedContainerView.h"
#import "MMPhoneCodePicker.h"
#import "MMLocationManager.h"

#import "MMEnterVerificationCodeViewController.h"

#import "NSString+rev.h"

typedef NS_ENUM(NSInteger, MMCreateUserIDViewControllerUserIDType) {
    MMCreateUserIDViewControllerUserIDTypeEmail,
    MMCreateUserIDViewControllerUserIDTypeMobile
};

@interface MMCreateUserIDViewController () < MMPhoneCodePickerDelegate, MMEnterVerificationCodeViewControllerDelegate >
    
@property (weak, nonatomic) IBOutlet MMSignUpTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIView *emailTextFieldContainerView;
@property (weak, nonatomic) IBOutlet UIView *mobileTextFieldContainerView;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *phoneCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (nonatomic, strong) MMPhoneCodePicker* phoneCodePicker;
@property (weak, nonatomic) IBOutlet UIImageView *countryFlagImageView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *userIDSegmentedControl;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIView *verificationCodeContainerView;
@property (nonatomic, strong) MMEnterVerificationCodeViewController* enterVerificationCodeViewController;

@end

@implementation MMCreateUserIDViewController

#pragma mark - UIViewController Life Cycle
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Create UserID";

    [self _setupNavigationItems];
    [self _setupTextFields];
    [self _setupSegmenetedControl];
    
    [[MMLocationManager sharedManager] refreshLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_prefillTextfieldsWithLocation:)
                                                 name:kLocationManagerDidChangeLocationNotification
                                               object:nil];

}
    
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[MMEnterVerificationCodeViewController class]]) {
        _enterVerificationCodeViewController = (MMEnterVerificationCodeViewController *)segue.destinationViewController;
        _enterVerificationCodeViewController.delegate = self;
    }
}

#pragma mark - Setup

- (void)_setupSegmenetedControl {
    [_userIDSegmentedControl setTitleTextAttributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15.0f],
                                                     NSForegroundColorAttributeName : [UIColor whiteColor]
                                                     }
                                          forState:UIControlStateNormal];
    [_userIDSegmentedControl setTitleTextAttributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15.0f],
                                                     NSForegroundColorAttributeName : [UIColor whiteColor]
                                                     }
                                          forState:UIControlStateSelected];
}

- (void)_setupTextFields {
    _phoneCodePicker = [MMPhoneCodePicker new];
    _phoneCodePicker.phoneCodePickerDelegate = self;
    _phoneCodeTextField.inputView = _phoneCodePicker;
    _phoneCodeTextField.inputAccessoryView = [self _toolbarWithTitle:NSLocalizedString(@"Please select your phone code:", nil)];
    _phoneCodeTextField.inputAssistantItem.leadingBarButtonGroups = @[];
    _phoneCodeTextField.inputAssistantItem.trailingBarButtonGroups = @[];
}

- (void)_setupNavigationItems {
    UIImage* backButtonImage = [[UIImage imageNamed:@"BtnBackAuth"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.imageView.contentMode = UIViewContentModeCenter;
    backButton.frame = CGRectMake(0.0f, 0.0f, 84.0f, 84.0f);
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0, 0.0f)];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* cardBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    cardBarButtonItem.tintColor = [UIColor redColor];
    self.navigationItem.leftBarButtonItem = cardBarButtonItem;
}

- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Utils

- (UIToolbar *)_toolbarWithTitle:(NSString *)title {
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 44.0f)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.translucent = YES;
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:toolBar.bounds];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [toolBar addSubview:titleLabel];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(donePickerViewButtonAction:)];
    [toolBar setItems:@[doneButton]];
    return toolBar;
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    
    if (textField == _emailTextField || textField == _phoneNumberTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

#pragma mark - Actions

- (IBAction)sendVerificationCodeButtonAction:(id)sender {
    
    MMCreateUserIDViewControllerUserIDType userIDType = (MMCreateUserIDViewControllerUserIDType)_userIDSegmentedControl.selectedSegmentIndex;
    if (userIDType == MMCreateUserIDViewControllerUserIDTypeEmail) {
        //TODO: Send validation code on email.

    } else {
        //TODO: Send validation code on mobile phone.
    }
    
    [self.view endEditing:YES];
    
    _verificationCodeContainerView.hidden = NO;
    [_enterVerificationCodeViewController becomeFirstResponder];
}

- (IBAction)userIDSegmentedControlValueChanged:(UISegmentedControl *)sender {
    MMCreateUserIDViewControllerUserIDType userIDType = (MMCreateUserIDViewControllerUserIDType)sender.selectedSegmentIndex;
    if (userIDType == MMCreateUserIDViewControllerUserIDTypeEmail) {
        _mobileTextFieldContainerView.hidden = YES;
        _emailTextFieldContainerView.hidden = NO;
    } else {
        _mobileTextFieldContainerView.hidden = NO;
        _emailTextFieldContainerView.hidden = YES;
    }
    
    [self.view endEditing:YES];
}

- (void)donePickerViewButtonAction:(id)sender {
    [self.view endEditing:YES];
    
    [_phoneNumberTextField becomeFirstResponder];
}

#pragma mark - MMPhoneCodePickerDelegate

- (void)phoneCodePicker:(MMPhoneCodePicker *)picker didSelectPhoneCode:(MMCountryPhoneCode *)phoneCode {
    _phoneCodeTextField.text = [NSString stringWithFormat:@"+%@", phoneCode.phoneCode];
    _countryFlagImageView.image = [UIImage imageNamed:phoneCode.countryCode];
}

#pragma mark - Notifications

- (void)_prefillTextfieldsWithLocation:(NSNotification *)notification {
    if (notification.object) {
        CLLocation* currentLocation = [notification object];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil && [placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks lastObject];
                NSString* countryCode = placemark.ISOcountryCode;
                _phoneCodePicker.selectedPhoneCode = [MMPhoneCodePicker phoneCodeForCountryCode:countryCode];
                _phoneCodeTextField.text = [NSString stringWithFormat:@"+%@", _phoneCodePicker.selectedPhoneCode.phoneCode];
                _countryFlagImageView.image = [UIImage imageNamed:countryCode];
            }
        }];
    }
}

#pragma mark - MMEnterVerificationCodeViewControllerDelegate

- (void)enterVerificationCodeViewController:(MMEnterVerificationCodeViewController *)vc didFinishEnteringCode:(NSString *)verificationCode {
    NSLog(@"User did enter verification code: %@", verificationCode);
    
    MMCreateAccountViewController* createAccountVC = [[UIStoryboard storyboardWithName:@"Auth" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([MMCreateAccountViewController class])];
    if (_userIDSegmentedControl.selectedSegmentIndex == MMCreateUserIDViewControllerUserIDTypeEmail) {
        createAccountVC.userID = _emailTextField.text;
    } else {
        //TODO: Set correct phone number format.
        createAccountVC.userID = _phoneNumberTextField.text;
    }
    [self.navigationController pushViewController:createAccountVC animated:YES];
}

@end

//
//  MMProvince.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/27/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMProvince.h"

@implementation MMProvince

#pragma mark - Initializers

- (instancetype)initWithDictionary:(NSDictionary *)dict {
  self = [super init];
  if (self) {
    _countryCode = [dict objectForKey:@"country"];
    _provinceShort = [dict objectForKey:@"short"];
    _province = [dict objectForKey:@"name"];
  }
  return self;
}


@end

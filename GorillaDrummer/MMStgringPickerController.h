//
//  MMStgringsPicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/31/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMStgringPickerController : UIViewController

@property (nonatomic, strong) NSArray<NSString *> *listOfStrings;

@property (nonatomic, assign) NSInteger selectedIndex;

@property (nonatomic, assign) BOOL showGradientOvelay;

- (void)didSelectString:(NSString *)selectedString;

@end

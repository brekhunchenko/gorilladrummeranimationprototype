//
//  MMEnterVerificationCodeViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 1/7/18.
//  Copyright © 2018 Yalex Mobile. All rights reserved.
//

#import "MMEnterVerificationCodeViewController.h"
#import "MMSignUpTextField.h"

@interface MMEnterVerificationCodeViewController () < UITextFieldDelegate >

@property (weak, nonatomic) IBOutlet MMSignUpTextField *textField_0;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *textField_1;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *textField_2;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *textField_3;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *textField_4;
@property (weak, nonatomic) IBOutlet MMSignUpTextField *textField_5;

@end

@implementation MMEnterVerificationCodeViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self _setupTextFields];
}

#pragma mark - Setup

- (void)_setupTextFields {
    [_textField_0 addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [_textField_1 addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [_textField_2 addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [_textField_3 addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [_textField_4 addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [_textField_5 addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - UITextField

- (MMSignUpTextField *)_nextTextFieldForTextField:(MMSignUpTextField *)textField {
    MMSignUpTextField* nextTextField = (MMSignUpTextField *)[self.view viewWithTag:(textField.tag + 1)];
    if ([nextTextField isMemberOfClass:[MMSignUpTextField class]]) {
        return nextTextField;
    } else {
        return nil;
    }
}

- (MMSignUpTextField *)_previousTextFieldForTextField:(MMSignUpTextField *)textField {
    MMSignUpTextField* previousTextField = (MMSignUpTextField *)[self.view viewWithTag:(textField.tag - 1)];
    if ([previousTextField isMemberOfClass:[MMSignUpTextField class]]) {
        return previousTextField;
    } else {
        return nil;
    }
}

- (void)textChanged:(MMSignUpTextField *)textField {
    if (textField.text.length > 1) {
        textField.text = [textField.text substringFromIndex:(textField.text.length - 1)];
    }
    
    BOOL movingToNextTextField = textField.text.length >= 1;
    if (movingToNextTextField) {
        MMSignUpTextField* nextTextField = [self _nextTextFieldForTextField:textField];
        [self.view endEditing:YES];
        [nextTextField becomeFirstResponder];
    }
    
    if ([self _userDidEnterAllDigits]) {
        if ([self.delegate respondsToSelector:@selector(enterVerificationCodeViewController:didFinishEnteringCode:)]) {
            [self.delegate enterVerificationCodeViewController:self didFinishEnteringCode:@""];
        }
    }
}

- (BOOL)_userDidEnterAllDigits {
    return (_textField_0.text.length && _textField_1.text.length && _textField_2.text.length && _textField_3.text.length && _textField_4.text.length && _textField_5.text.length);
}

- (NSString *)verificationCode {
    if ([self _userDidEnterAllDigits]) {
        NSMutableString* verificationCodeString = [NSMutableString new];
        [verificationCodeString appendString:_textField_0.text];
        [verificationCodeString appendString:_textField_1.text];
        [verificationCodeString appendString:_textField_2.text];
        [verificationCodeString appendString:_textField_3.text];
        [verificationCodeString appendString:_textField_4.text];
        [verificationCodeString appendString:_textField_5.text];
        return verificationCodeString;
    } else {
        return @"";
    }
}

- (BOOL)becomeFirstResponder {
    [_textField_0 becomeFirstResponder];
    return NO;
}

@end

//
//  MMCoinsBarButtonItem.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCoinsBarButtonItem : UIBarButtonItem

- (nullable MMCoinsBarButtonItem *)initWithTarget:(nullable id)target
                                           action:(nullable SEL)action;

@property (nonatomic, assign) NSInteger numberOfCoinsAvailable;

@end

@interface MMCoinsButton : UIButton

@property (nonatomic, assign) NSInteger numberOfCoinsAvailable;

@end

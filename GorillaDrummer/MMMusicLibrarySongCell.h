//
//  MMMusicLibrarySongCell.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicLibraryDefines.h"
#import "MGSwipeCollectionViewCell.h"

@interface MMMusicLibrarySongCell : MGSwipeCollectionViewCell

@property (nonatomic, assign) SongDifficulty songDifficulty;
@property (nonatomic, assign) BOOL songAvailable;

+ (CGFloat)cellHeight;

@end

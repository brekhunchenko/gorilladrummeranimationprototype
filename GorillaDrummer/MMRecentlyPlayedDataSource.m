//
//  MMRecentlyPlayedDataSource.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMRecentlyPlayedDataSource.h"
#import "MMRecentlyPlayedCell.h"

@implementation MMRecentlyPlayedDataSource

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  NSInteger numberOfItems = 30;
  return numberOfItems;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MMRecentlyPlayedCell* headerCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MMRecentlyPlayedCell class])
                                                                               forIndexPath:indexPath];
  headerCell.songDifficulty = rand()%4;
  return headerCell;
}

@end

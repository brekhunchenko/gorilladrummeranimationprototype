//
//  MMTextureManager.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

#import "GorillaDrummerDefines.h"

@interface MMTextureManager : NSObject

+ (instancetype)sharedInstance;

- (void)preloadTextures;

- (NSArray<SKTexture *> *)gorillaFaceTexturesForExpression:(GorillaDrummerExpression)expression;
- (NSArray<SKTexture *> *)gorillaBeatAnimationTexturesForType:(GorillaDrummerBeatType)beatType;
- (NSArray<SKTexture *> *)confettiAnimation;

@end

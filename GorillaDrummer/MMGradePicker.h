//
//  MMGradePicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/31/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMStgringPickerController.h"

@protocol MMGradePickerDelegate;

@interface MMGradePicker : MMStgringPickerController

@property (nonatomic, weak) id<MMGradePickerDelegate> gradePickerDelegate;
@property (nonatomic, strong) NSString* grade;

+ (NSArray <NSString *> *)listOfGrades;

@end

@protocol MMGradePickerDelegate <NSObject>

- (void)gradePicker:(MMGradePicker *)gradePicker didSelectGrade:(NSString *)grade;

@end

//
//  MMCountryPicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/17/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMCountryPicker.h"

@interface MMCountryPicker () <UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation MMCountryPicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupCountries];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupCountries];
}

#pragma mark - Setup

- (void)_setupCountries {
  self.dataSource = self;
  self.delegate = self;
}

#pragma mark - Custom Setters & Getters

- (void)setSelectedCountryCode:(NSString *)countryCode {
  [self setSelectedCountryCode:countryCode animated:NO];
}

- (void)setSelectedCountryCode:(NSString *)countryCode animated:(BOOL)animated {
  NSUInteger index = [[[self class] countryCodes] indexOfObject:countryCode];
  if (index != NSNotFound) {
    [self selectRow:(NSInteger)index inComponent:0 animated:animated];
  } else {
    NSLog(@"Error: something went wrong.");
  }
}

- (NSString *)selectedCountryCode {
  NSUInteger index = (NSUInteger)[self selectedRowInComponent:0];
  return [[self class] countryCodes][index];
}

- (void)setSelectedCountryName:(NSString *)countryName animated:(BOOL)animated {
  NSUInteger index = [[[self class] countryNames] indexOfObject:countryName];
  if (index != NSNotFound) {
    [self selectRow:(NSInteger)index inComponent:0 animated:animated];
  }
}

- (void)setSelectedCountryName:(NSString *)countryName {
  [self setSelectedCountryName:countryName animated:NO];
}

- (NSString *)selectedCountryName {
  NSUInteger index = (NSUInteger)[self selectedRowInComponent:0];
  return [[self class] countryNames][index];
}

#pragma mark - UIPickerView

- (NSInteger)numberOfComponentsInPickerView:(__unused UIPickerView *)pickerView {
  return 1;
}

- (NSInteger)pickerView:(__unused UIPickerView *)pickerView numberOfRowsInComponent:(__unused NSInteger)component {
  return (NSInteger)[[self class] countryCodes].count;
}

- (UIView *)pickerView:(__unused UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(__unused NSInteger)component reusingView:(UIView *)view {
  if (!view) {
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
    label.backgroundColor = [UIColor clearColor];
    label.tag = 1;
    [view addSubview:label];
    
    UIImageView *flagView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
    flagView.contentMode = UIViewContentModeScaleAspectFit;
    flagView.tag = 2;
    [view addSubview:flagView];
  }
  
  ((UILabel *)[view viewWithTag:1]).text = [[self class] countryNames][(NSUInteger)row];
  NSString *countryCode = [[self class] countryCodes][(NSUInteger) row];
  UIImage *image = [UIImage imageNamed:countryCode];
  ((UIImageView *)[view viewWithTag:2]).image = image;
  return view;
}

- (void)pickerView:(__unused UIPickerView *)pickerView
      didSelectRow:(__unused NSInteger)row
       inComponent:(__unused NSInteger)component {
  if ([self.countryPickerDelegate respondsToSelector:@selector(countryPicker:didSelectCountryWithCode:countryName:)]) {
    
    self.selectedCountryCode = [self.class countryCodes][row];
    self.selectedCountryName = [self.class countryNames][row];
    
    [self.countryPickerDelegate countryPicker:self
                     didSelectCountryWithCode:self.selectedCountryCode
                                  countryName:self.selectedCountryName];
  }
}

#pragma mark - Utilz

+ (NSArray *)countryNames {
  static NSArray *_countryNames = nil;
  if (!_countryNames) {
    __block NSMutableArray* countryNamesMutable = [[self countryNamesByCode].allValues mutableCopy];
    void (^moveCountryWithNameOnTop)(NSString*countryName) = ^(NSString* countryName) {
      NSInteger idx = NSNotFound;
      idx = [countryNamesMutable indexOfObject:countryName];
      if (idx != NSNotFound) {
        [countryNamesMutable removeObjectAtIndex:idx];
        [countryNamesMutable insertObject:countryName atIndex:0];
      }
    };
    
    moveCountryWithNameOnTop(@"Japan");
    moveCountryWithNameOnTop(@"South Korea");
    moveCountryWithNameOnTop(@"Hong Kong (China)");
    moveCountryWithNameOnTop(@"China");
    moveCountryWithNameOnTop(@"United Kingdom");
    moveCountryWithNameOnTop(@"United States");
  
    
    _countryNames = countryNamesMutable;
  }
  return _countryNames;
}

+ (NSArray *)countryCodes {
  static NSArray *_countryCodes = nil;
  if (!_countryCodes) {
    _countryCodes = [[[self countryCodesByName] objectsForKeys:[self countryNames] notFoundMarker:@""] copy];
  }
  return _countryCodes;
}

+ (NSDictionary *)countryNamesByCode {
  static NSDictionary *_countryNamesByCode = nil;
  if (!_countryNamesByCode) {
    NSMutableDictionary *namesByCode = [NSMutableDictionary dictionary];
    for (NSString *code in [NSLocale ISOCountryCodes]) {
      NSString *countryName = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:code];
      if (!countryName) {
        countryName = [[NSLocale localeWithLocaleIdentifier:@"en_US"] displayNameForKey:NSLocaleCountryCode value:code];
      }
      
      namesByCode[code] = countryName ?: code;
    }
    _countryNamesByCode = [namesByCode copy];
  }
  return _countryNamesByCode;
}

+ (NSDictionary *)countryCodesByName {
  static NSDictionary *_countryCodesByName = nil;
  if (!_countryCodesByName) {
    NSDictionary *countryNamesByCode = [self countryNamesByCode];
    NSMutableDictionary *codesByName = [NSMutableDictionary dictionary];
    for (NSString *code in countryNamesByCode) {
      codesByName[countryNamesByCode[code]] = code;
    }
    _countryCodesByName = [codesByName copy];
  }
  return _countryCodesByName;
}

+ (NSString *)countryNameForCountryCode:(NSString *)countryCode {
  NSInteger idxOfCountryCode = [[self countryCodes] indexOfObject:countryCode];
  if (idxOfCountryCode != NSNotFound) {
    return [[self countryNames] objectAtIndex:idxOfCountryCode];
  } else {
    return nil;
  }
}

@end

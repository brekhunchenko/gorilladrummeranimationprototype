//
//  MMRecentlyPlayedCell.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicLibraryDefines.h"

@interface MMRecentlyPlayedCell : UICollectionViewCell

@property (nonatomic, assign) SongDifficulty songDifficulty;

@end

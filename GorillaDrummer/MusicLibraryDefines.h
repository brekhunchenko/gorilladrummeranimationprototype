//
//  MusicLibraryDefines.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/24/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#ifndef MusicLibraryDefines_h
#define MusicLibraryDefines_h

typedef NS_ENUM(NSInteger, SongDifficulty) {
  SongDifficultyBeginner,
  SongDifficultyEasy,
  SongDifficultyModerate,
  SongDifficultyAdvanced
};

#endif /* MusicLibraryDefines_h */

//
//  MMGradePicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/31/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMGradePicker.h"

@implementation MMGradePicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupPicker];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupPicker];
}

#pragma mark - Setup

- (void)_setupPicker {
  self.listOfStrings = [self.class listOfGrades];
}

#pragma mark - Custom Setters & Getters

- (void)setGrade:(NSString *)grade {
  if (![grade isEqualToString:_grade]) {
    _grade = grade;
    
    __block NSInteger gradingExamIdx = NSNotFound;
    [[self.class listOfGrades] enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
      if ([obj isEqualToString:_grade]) {
        gradingExamIdx = idx;
        *stop = YES;
      }
    }];
    
    if (gradingExamIdx != NSNotFound) {
      [self setSelectedIndex:gradingExamIdx];
    } else {
      NSLog(@"Error: province not found.");
    }
  }
}

#pragma mark - Overridden

- (void)didSelectString:(NSString *)selectedString {
  if ([self.gradePickerDelegate respondsToSelector:@selector(gradePicker:didSelectGrade:)]) {
    [self.gradePickerDelegate gradePicker:self didSelectGrade:selectedString];
  }
}

#pragma mark - Class Methods

+ (NSArray<NSString *> *)listOfGrades {
  static NSArray<NSString *> *llistOfGrades = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    llistOfGrades = @[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
  });
  return llistOfGrades;
}

@end

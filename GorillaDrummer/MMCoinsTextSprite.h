//
//  MMCoinsTextSprite.h
//  GorillaDrummer
//
//  Created by Yaroslav on 11/1/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MMCoinsTextSprite : SKSpriteNode

- (void)setText:(NSString *)text numberOfCoins:(NSInteger)numberOfCoins;

@end

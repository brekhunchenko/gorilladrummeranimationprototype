//
//  MMClassStandingCell.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/25/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMClassStandingCell.h"

@interface MMClassStandingCell()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statsLabel;

@end

@implementation MMClassStandingCell

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  self.selectionStyle = UITableViewCellSelectionStyleNone;
  
  [self _setupAvatarImageView];
  
  _nameLabel.text = [NSString stringWithFormat:@"Yushen Han"];
  _statsLabel.text = [NSString stringWithFormat:@"%d coins, %d pitch badges", 300, 4];
}

#pragma mark - Setup

- (void)_setupAvatarImageView {
  _avatarImageView.layer.cornerRadius = 24.0f;
  _avatarImageView.layer.masksToBounds = YES;
}

@end

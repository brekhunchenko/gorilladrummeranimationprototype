//
//  GameViewController.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

#import "GorillaDrummerDefines.h"

@interface MMGorillaDrummerViewController : UIViewController

- (void)updateSceneStyle:(GorillaDrummerSceneStyle)style;

@end

//
//  MMCityPicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/28/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCity.h"

@protocol MMCityPickerDelegate;

@interface MMCityPicker : UIPickerView

@property (nonatomic, weak) id<MMCityPickerDelegate> cityPickerDelegate;

- (void)setupCitiesForCountryName:(NSString *)countryName;
@property (nonatomic, strong, readonly) NSString* countryName;
@property (nonatomic, strong) MMCity* city;

+ (NSArray <MMCity *> *)citiesForCountryName:(NSString *)countryName;
+ (MMCity *)cityForCityName:(NSString *)cityName countyName:(NSString *)countryName;

@end

@protocol MMCityPickerDelegate <NSObject>

- (void)cityPicker:(MMCityPicker *)cityPicker didSelectCity:(MMCity *)city;

@end

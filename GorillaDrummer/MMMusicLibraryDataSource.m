//
//  MMMusicLibraryDataSource.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMMusicLibraryDataSource.h"
#import "MMMusicLibraryHeaderCell.h"
#import "MMMusicLibrarySongCell.h"

@interface MMMusicLibraryDataSource()

@end

@implementation MMMusicLibraryDataSource

#pragma mark - Custom Setters & Getters

- (void)setSongsSection:(MMMusicLibrarySongsSection)songsSection {
  _songsSection = songsSection;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return kMMMusicLibraryNumberOfSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  NSInteger numberOfItems = 0;
  if (section == MMMusicLibrarySectionHeader) {
    numberOfItems = 1;
  } else if (section == MMMusicLibrarySectionAllSongsAndAssignements){
    numberOfItems = 100;
  }
  return numberOfItems;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section == MMMusicLibrarySectionHeader) {
    MMMusicLibraryHeaderCell* headerCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MMMusicLibraryHeaderCell class])
                                                                                     forIndexPath:indexPath];
    headerCell.showRecentlyPlayed = self.showRecentlyPlayedMusic;
    return headerCell;
  } else if (indexPath.section == MMMusicLibrarySectionAllSongsAndAssignements) {
    MMMusicLibrarySongCell* songCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MMMusicLibrarySongCell class])
                                                                                 forIndexPath:indexPath];
    //TODO: Set song depends on selected songs section
    songCell.songDifficulty = rand()%4;
    songCell.songAvailable = (rand()%2 == 0);
    return songCell;
  }
  return nil;
}

@end

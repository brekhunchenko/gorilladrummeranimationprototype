//
//  MMInstrumentsPicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/28/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMInstrumentPicker.h"

@interface MMInstrumentPicker()

@end

@implementation MMInstrumentPicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupPicker];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupPicker];
}

#pragma mark - Setup

- (void)_setupPicker {
  self.listOfStrings = [self.class listOfMusicInstruments];
}

#pragma mark - Custom Setters & Getters

- (void)setInstrument:(NSString *)instrument {
  if (![instrument isEqualToString:_instrument]) {
    _instrument = instrument;
    
    __block NSInteger instrumentIdx = NSNotFound;
    [[self.class listOfMusicInstruments] enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
      if ([obj isEqualToString:instrument]) {
        instrumentIdx = idx;
        *stop = YES;
      }
    }];
    
    if (instrumentIdx != NSNotFound) {
      [self setSelectedIndex:instrumentIdx];
    } else {
      NSLog(@"Error: province not found.");
    }
  }
}

#pragma mark - Overridden

- (void)didSelectString:(NSString *)selectedString {
  if ([self.instrumentsPickerDelegate respondsToSelector:@selector(instrumentsPicker:didSelectInstrument:)]) {
    [self.instrumentsPickerDelegate instrumentsPicker:self didSelectInstrument:selectedString];
  }
}

#pragma mark - Class Methods

+ (NSArray<NSString *> *)listOfMusicInstruments {
  return @[@"Violin",
           @"Viola",
           @"Cello",
           @"Oboe",
           @"Flute",
           @"Clarinet",
           @"Bassoon",
           @"Recorder",
           @"Saxophone",
           @"Trumpet",
           @"Horn",
           @"Tuba",
           @"Trombone",
           @"Guitar",
           @"Piano",
           @"Percussion",
           @"Vocal"];
}

@end

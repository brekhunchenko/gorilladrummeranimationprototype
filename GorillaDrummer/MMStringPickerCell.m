//
//  MMStringPickerCell.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 12/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMStringPickerCell.h"

@interface MMStringPickerCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation MMStringPickerCell

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  self.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma mark - Custom Setters & Getters

- (void)setTextString:(NSString *)textString selected:(BOOL)selected {
  _textString = textString;
  _stringSelected = selected;
  
  _titleLabel.text = _textString;
  _titleLabel.textColor = selected ? [UIColor colorWithRed:33.0f/255.0f
                                                     green:136.0f/255.0f
                                                      blue:207.0f/255.0f
                                                     alpha:1.0f] : [UIColor darkGrayColor];

}

@end

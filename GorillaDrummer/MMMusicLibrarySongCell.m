//
//  MMMusicLibrarySongCell.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMMusicLibrarySongCell.h"

@interface MMMusicLibrarySongCell()

@property (weak, nonatomic) IBOutlet UIImageView *musicNoteIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfCoinsLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrangementNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *downloadIcon;

@end

@implementation MMMusicLibrarySongCell

#pragma mark - Class Methods

+ (CGFloat)cellHeight {
  return 80.0f;
}

#pragma mark - Custom Setters & Getters

- (void)setSongDifficulty:(SongDifficulty)songDifficulty {
  _songDifficulty = songDifficulty;
  
  UIImage* difficultyImage = nil;
  switch (songDifficulty) {
    case SongDifficultyEasy: {
      difficultyImage = [UIImage imageNamed:@"Easy"];
    }
      break;
    case SongDifficultyModerate: {
      difficultyImage = [UIImage imageNamed:@"Moderate"];
    }
      break;
    case  SongDifficultyBeginner: {
      difficultyImage = [UIImage imageNamed:@"Icon-MusicNote-Basic"];
    }
      break;
    case SongDifficultyAdvanced: {
      difficultyImage = [UIImage imageNamed:@"Icon-MusicNote-Hard"];
    }
      break;
  }
  _musicNoteIconImageView.image = difficultyImage;
}

- (void)setSongAvailable:(BOOL)songAvailable {
  _songAvailable = songAvailable;
  
  if (_songAvailable) {
    _arrangementNameLabel.alpha = 1.0f;
    _detailsLabel.alpha = 0.6f;
    _musicNoteIconImageView.alpha = 1.0f;
    _downloadIcon.hidden = YES;
    _numberOfCoinsLabel.hidden = NO;
  } else {
    _arrangementNameLabel.alpha = 0.5f;
    _detailsLabel.alpha = 0.5f;
    _musicNoteIconImageView.alpha = 0.5f;
    _downloadIcon.hidden = NO;
    _numberOfCoinsLabel.hidden = YES;
  }
}

@end

//
//  MMMusicLibrarySearchFilterPickerView.m
//  GorillaDrummer
//
//  Created by Yaroslav on 11/1/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMMusicLibrarySearchFilterPickerViewController.h"

@interface MMMusicLibrarySearchFilterPickerViewController () < UITableViewDataSource, UITableViewDelegate >

@property (nonatomic, strong) NSArray<NSString *>* listOfStrings;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MMMusicLibrarySearchFilterPickerViewController

#pragma mark - Initializers

+ (MMMusicLibrarySearchFilterPickerViewController *)musicLibrarySearchFilterPickerViewController:(CGRect)frame
                                                               listOfStrings:(NSArray<NSString *> *)listOfStrings {
    MMMusicLibrarySearchFilterPickerViewController* viewController = [[NSBundle mainBundle] loadNibNamed:@"MMMusicLibrarySearchFilterPickerViewController"
                                                                             owner:nil
                                                                           options:nil].firstObject;
    viewController.view.frame = frame;

    viewController.listOfStrings = listOfStrings;
    viewController.recentlySelectedFilterString = @"";
    
    viewController.tableView.delegate = viewController;
    viewController.tableView.dataSource = viewController;
    [viewController.tableView reloadData];
    
    viewController.view.layer.cornerRadius = 6.0f;
    viewController.view.layer.masksToBounds = YES;
    
    return viewController;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listOfStrings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MMMusicLibrarySearchFilterCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MMMusicLibrarySearchFilterCell class])];
    if (cell == nil) {
        cell = [[MMMusicLibrarySearchFilterCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:NSStringFromClass([MMMusicLibrarySearchFilterCell class])];
    }
    NSString* filterString = _listOfStrings[indexPath.row];
    cell.textLabel.text = filterString;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    BOOL selected = [filterString isEqualToString:_recentlySelectedFilterString];
    cell.textLabel.textColor = selected ? [UIColor colorWithRed:33.0f/255.0f
                                                          green:136.0f/255.0f
                                                           blue:207.0f/255.0f
                                                          alpha:1.0f] : [UIColor darkGrayColor];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* filterString = _listOfStrings[indexPath.row];
    _recentlySelectedFilterString = filterString;
    [_tableView reloadData];
    if ([self.delegate respondsToSelector:@selector(musicLibrarySearchFilterPickerViewController:didSelectItem:)]) {
        [self.delegate musicLibrarySearchFilterPickerViewController:self didSelectItem:filterString];
    }
}

@end

@implementation MMMusicLibrarySearchFilterCell

#pragma mark - Overridden

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.textLabel.frame = CGRectMake(0.0f,
                                      self.textLabel.frame.origin.y,
                                      self.bounds.size.width,
                                      self.textLabel.frame.size.height);
}

@end

//
//  LocationManager.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/18/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMLocationManager.h"

NSString * const kLocationManagerDidChangeLocationNotification = @"kLocationManagerDidChangeLocationNotification";

@interface MMLocationManager() <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, strong, readwrite) CLLocation* userLocation;

@end

@implementation MMLocationManager

+ (instancetype)sharedManager {
  
  static id __sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    __sharedManager = [[self alloc] init];
  });
  return __sharedManager;
}

- (instancetype)init {
  
  self = [super init];
  
  if (self) {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter  = 500.0;
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
      [self.locationManager requestWhenInUseAuthorization];
    }
  }
  
  return self;
}

- (void)startLocationUpdates {
  [self.locationManager startUpdatingLocation];
  [self.locationManager startMonitoringSignificantLocationChanges];
}

- (void)refreshLocation {
  [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
  
  if (locations.count) {
    _userLocation = [locations lastObject];
    [self.locationManager stopUpdatingLocation];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidChangeLocationNotification
                                                        object:_userLocation];
  }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
  NSLog(@"Error: %@", error);
}


@end

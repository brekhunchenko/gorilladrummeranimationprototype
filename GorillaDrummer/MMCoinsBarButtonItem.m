//
//  MMCoinsBarButtonItem.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMCoinsBarButtonItem.h"
#import "MMNavigationBar.h"

@interface MMCoinsBarButtonItem()

@property (nonatomic, strong) UIButton* numberOfCoinsButton;

@end

@implementation MMCoinsBarButtonItem

#pragma mark - Initializers

- (instancetype)initWithTarget:(nullable id)target
                        action:(nullable SEL)action {
  _numberOfCoinsButton = [MMCoinsButton buttonWithType:UIButtonTypeCustom];
  _numberOfCoinsButton.frame = CGRectMake(0.0f, 0.0f, 90.0f, kMMNavigationBarHeight);
  
  MMCoinsBarButtonItem* cardBarButtonItem = [[MMCoinsBarButtonItem alloc] initWithCustomView:_numberOfCoinsButton];
  cardBarButtonItem.target = target;
  cardBarButtonItem.action = action;
  cardBarButtonItem.tintColor = [UIColor redColor];
  cardBarButtonItem.numberOfCoinsButton = _numberOfCoinsButton;
  
  [_numberOfCoinsButton addTarget:cardBarButtonItem
                           action:@selector(coinsButtonPressed:)
                 forControlEvents:UIControlEventTouchUpInside];
  return cardBarButtonItem;
}

#pragma mark - Custom Setters & Getters

- (void)setNumberOfCoinsAvailable:(NSInteger)numberOfCoinsAvailable {
  [_numberOfCoinsButton setTitle:[NSString stringWithFormat:@"%ld", numberOfCoinsAvailable]
                        forState:UIControlStateNormal];
}

#pragma mark - Actions

- (void)coinsButtonPressed:(id)sender {
  [[UIApplication sharedApplication] sendAction:self.action
                                             to:self.target
                                           from:nil
                                       forEvent:nil];
}

@end

@implementation MMCoinsButton

#pragma mark - Initialziers

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self _initialSetup];
  }
  return self;
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType {
  MMCoinsButton* btn = [[MMCoinsButton alloc] initWithFrame:CGRectZero];
  return btn;
}

#pragma mark - Setup

- (void)_initialSetup {
  self.titleLabel.font = [UIFont boldSystemFontOfSize:22.0f];
  [self setTitle:@"1"
        forState:UIControlStateNormal];
  self.titleLabel.textAlignment = NSTextAlignmentRight;
  self.titleLabel.backgroundColor = [UIColor clearColor];
  [self setTitleColor:[UIColor colorWithRed:255.0f/255.0f green:226.0f/255.0f blue:77.0f/255.0f alpha:1.0]
             forState:UIControlStateNormal];
  [self setImage:[UIImage imageNamed:@"Coins"]
        forState:UIControlStateNormal];
  self.imageView.contentMode = UIViewContentModeScaleAspectFit;
  self.imageView.backgroundColor = [UIColor clearColor];
  self.reversesTitleShadowWhenHighlighted = YES;
}

#pragma mark - Custom Setters & Getters

- (void)setNumberOfCoinsAvailable:(NSInteger)numberOfCoinsAvailable {
  _numberOfCoinsAvailable = numberOfCoinsAvailable;
  
  [self setTitle:[NSString stringWithFormat:@"%d", (int)_numberOfCoinsAvailable]
        forState:UIControlStateNormal];
}

#pragma mark - Overridden

- (void)layoutSubviews {
  [super layoutSubviews];
  
  CGSize sizeThatFits = [self.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber10_11_Max) {
    self.titleLabel.frame = CGRectMake(self.frame.size.width - sizeThatFits.width,
                                       -20.0f,
                                       sizeThatFits.width,
                                       self.frame.size.height);
  } else {
    self.titleLabel.frame = CGRectMake(self.frame.size.width - sizeThatFits.width,
                                       -4.0f,
                                       sizeThatFits.width,
                                       self.frame.size.height);
  }
  CGSize coinsImageViewSize = CGSizeMake(60.0f, 46.5f);
  CGFloat distanceBeetweenLabelAndImageView = 8.0f;
  self.imageView.frame = CGRectMake(self.frame.size.width - sizeThatFits.width - coinsImageViewSize.width - distanceBeetweenLabelAndImageView,
                                    0.0f,
                                    coinsImageViewSize.width,
                                    coinsImageViewSize.height);
}

@end

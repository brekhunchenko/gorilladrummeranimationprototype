//
//  MMGorillaDrummerSprite.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMGorillaDrummerSprite.h"
#import "MMTextureManager.h"

@interface MMGorillaDrummerSprite()

@property (nonatomic, strong) SKSpriteNode* headSpriteNode;

@end

@implementation MMGorillaDrummerSprite

#pragma mark - Initializers

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    for (SKNode* node in self.children) {
      if ([node.name isEqualToString:@"head"]) {
        _headSpriteNode = (SKSpriteNode *)node;
      }
    }
  }
  return self;
}

#pragma mark - Public Methods

- (void)showExpressionAnimation:(GorillaDrummerExpression)expression {
  if ([self actionForKey:@"expression_animation_action"]) {
    return;
  }
  
  NSArray<SKTexture *> *textures = [[MMTextureManager sharedInstance] gorillaFaceTexturesForExpression:expression];
  [_headSpriteNode runAction:[SKAction animateWithTextures:textures
                                   timePerFrame:1.0f/30.0f]
                     withKey:@"expression_animation_action"];
}

- (void)showBeatAnimation:(GorillaDrummerBeatType)beatType {
  if ([self actionForKey:@"beat_animation_action"]) {
    return;
  }
  
  NSArray<SKTexture *> *textures = [[MMTextureManager sharedInstance] gorillaBeatAnimationTexturesForType:beatType];
  [self runAction:[SKAction animateWithTextures:textures timePerFrame:1.0f/30.0f]
          withKey:@"beat_animation_action"];
  [_headSpriteNode runAction:[SKAction sequence:@[
                                                  [SKAction moveBy:CGVectorMake(0.0f, 6.0f) duration:0.3f],
                                                  [SKAction moveBy:CGVectorMake(0.0f, -6.0f) duration:0.3f]
                                                 ]]];
}

@end

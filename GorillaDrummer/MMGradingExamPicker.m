//
//  MMGradingExamPicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/31/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMGradingExamPicker.h"

@interface MMGradingExamPicker()
@end

@implementation MMGradingExamPicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupPicker];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupPicker];
}

#pragma mark - Setup

- (void)_setupPicker {
  self.listOfStrings = [self.class listOfGradingExams];
}

#pragma mark - Custom Setters & Getters

- (void)setGradingExam:(NSString *)gradingExam {
  if (![gradingExam isEqualToString:_gradingExam]) {
    _gradingExam = gradingExam;
    
    __block NSInteger gradingExamIdx = NSNotFound;
    [[self.class listOfGradingExams] enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
      if ([obj isEqualToString:gradingExam]) {
        gradingExamIdx = idx;
        *stop = YES;
      }
    }];
    
    if (gradingExamIdx != NSNotFound) {
      [self setSelectedIndex:gradingExamIdx];
    } else {
      NSLog(@"Error: province not found.");
    }
  }
}

#pragma mark - Overridden

- (void)didSelectString:(NSString *)selectedString {
  if ([self.gradingExamDelegate respondsToSelector:@selector(gradingExamPicker:didSelectGradingExam:)]) {
    [self.gradingExamDelegate gradingExamPicker:self didSelectGradingExam:selectedString];
  }
}

#pragma mark - Class Methods

+ (NSArray<NSString *> *)listOfGradingExams {
  static NSArray<NSString *> *listOfGradingExams = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    listOfGradingExams = @[@"ABRSM",
                           @"Suzuki",
                           @"CMA",
                           @"other"];
  });
  return listOfGradingExams;
}

@end

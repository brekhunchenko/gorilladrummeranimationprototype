//
//  MMNavigationBar.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMNavigationBar.h"

@implementation MMNavigationBar

#pragma mark - Initializers

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _initialSetup];
}

#pragma mark - Setup

- (void)_initialSetup {
  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber10_11_Max) {
      [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[MMNavigationBar class]]] setBackgroundVerticalPositionAdjustment:-18
                                                                                                                          forBarMetrics:UIBarMetricsDefault];
      [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[MMNavigationBar class]]] setBackButtonBackgroundVerticalPositionAdjustment:-18
                                                                                                                                    forBarMetrics:UIBarMetricsDefault];
      [self setTitleVerticalPositionAdjustment:-15.0f forBarMetrics:UIBarMetricsDefault];
  }
  self.translucent = NO;
  self.barStyle = UIBarStyleBlack;
  self.tintColor = [UIColor colorWithRed:26.0f/255.0f
                                   green:30.0f/255.0f
                                    blue:57.0f/255.0f
                                   alpha:1.0];
}

#pragma mark - Overridden

- (CGSize)sizeThatFits:(CGSize)size {
  CGSize newSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, kMMNavigationBarHeight);
  return newSize;
}

@end

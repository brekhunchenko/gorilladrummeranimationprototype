//
//  GorillaDrummerDefines.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#ifndef GorillaDrummerDefines_h
#define GorillaDrummerDefines_h

typedef NS_ENUM(NSInteger, GorillaDrummerExpression) {
  GorillaDrummerExpressionNeutral,
  GorillaDrummerExpressionHappy,
  GorillaDrummerExpressionDisappointed
};

typedef NS_ENUM(NSInteger, GorillaDrummerBeatType) {
  GorillaDrummerBeatType_0,
  GorillaDrummerBeatType_1,
  GorillaDrummerBeatType_2,
  GorillaDrummerBeatType_3,
  GorillaDrummerBeatType_Small0,
  GorillaDrummerBeatType_Small1
};

typedef NS_ENUM(NSInteger, GorillaDrummerSceneStyle) {
  GorillaDrummerSceneStyleBlue,
  GorillaDrummerSceneStyleGreen,
  GorillaDrummerSceneStyleYellow,
  GorillaDrummerSceneStyleRed,
  GorillaDrummerSceneStylePurple
};

#endif /* GorillaDrummerDefines_h */

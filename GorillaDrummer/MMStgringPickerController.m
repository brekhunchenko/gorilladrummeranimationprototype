//
//  MMStgringsPicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/31/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMStgringPickerController.h"
#import "MMStringPickerCell.h"

@interface MMStgringPickerController() < UITableViewDelegate, UITableViewDataSource >

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) UIImageView *bottomGradientOverlayImageView;

@end

@implementation MMStgringPickerController

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    self.selectedIndex = NSNotFound;
    self.showGradientOvelay = YES;
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
}

#pragma mark - UIViewController LifeCycle

- (void)loadView {
  [super loadView];
  
  [self _setupTableView];
  [self _setupOverlayGradientViews];
}

#pragma mark - Setup

- (void)_setupTableView {
  _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
  _tableView.dataSource = self;
  _tableView.delegate = self;
  _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  
  UINib* nib = [UINib nibWithNibName:NSStringFromClass([MMStringPickerCell class]) bundle:nil];
  [_tableView registerNib:nib forCellReuseIdentifier:NSStringFromClass([MMStringPickerCell class])];
  
  [self.view addSubview:_tableView];
}

- (void)_setupOverlayGradientViews {
  _bottomGradientOverlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_filter_gradient_bot"]];
  _bottomGradientOverlayImageView.frame = CGRectMake(0.0f, self.view.bounds.size.height - 190.0, self.view.bounds.size.width, 190.0);
  [self.view addSubview:_bottomGradientOverlayImageView];
  
  _bottomGradientOverlayImageView.hidden = !_showGradientOvelay;
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  
  _tableView.frame = self.view.bounds;
  _bottomGradientOverlayImageView.frame = CGRectMake(0.0f, self.view.bounds.size.height - 190.0, self.view.bounds.size.width, 190.0);
}

#pragma mark - Custom Setters & Getters

- (void)setShowGradientOvelay:(BOOL)showGradientOvelay {
  _showGradientOvelay = showGradientOvelay;
  
  _bottomGradientOverlayImageView.hidden = !_showGradientOvelay;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
  _selectedIndex = selectedIndex;
  
  [_tableView reloadData];
}

- (void)setListOfStrings:(NSArray<NSString *> *)listOfStrings {
  _listOfStrings = listOfStrings;
  
  [_tableView reloadData];
}

#pragma mark - UIPickerView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listOfStrings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  MMStringPickerCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MMStringPickerCell class])];
  NSString* string = _listOfStrings[indexPath.row];
  [cell setTextString:string selected:(indexPath.row == _selectedIndex)];
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  NSString* string = _listOfStrings[indexPath.row];
  [self didSelectString:string];
}

#pragma mark - 

- (void)didSelectString:(NSString *)selectedString {
  return;
}

@end

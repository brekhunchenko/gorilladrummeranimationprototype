//
//  GameViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMGorillaDrummerViewController.h"
#import "GorillaDrummerScene.h"
#import "MMGradientView.h"

#import <AVFoundation/AVFoundation.h>

@interface MMGorillaDrummerViewController()

@property (weak, nonatomic) IBOutlet SKView *gorillaSceneView;
@property (weak, nonatomic) IBOutlet MMGradientView *gradientView;
@property (weak, nonatomic) IBOutlet UIButton *coinsButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (nonatomic, strong) AVAudioRecorder *recorder;

@end

@implementation MMGorillaDrummerViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  [self _setupScene];
  [self _setupRecorder];
  [self updateSceneStyle:GorillaDrummerSceneStyleRed];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Setup

- (void)_setupScene {
  GorillaDrummerScene *scene = (GorillaDrummerScene *)[SKScene nodeWithFileNamed:@"GorillaDrummerScene"];
  scene.scaleMode = SKSceneScaleModeAspectFill;
  SKView *skView = (SKView *)self.gorillaSceneView;
  [skView presentScene:scene];
  skView.showsFPS = NO;
  skView.showsNodeCount = NO;
}

- (void)_setupRecorder {
  [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];

  NSDictionary *settings = @{AVSampleRateKey:          [NSNumber numberWithFloat: 44100.0],
                             AVFormatIDKey:            [NSNumber numberWithInt: kAudioFormatAppleLossless],
                             AVNumberOfChannelsKey:    [NSNumber numberWithInt: 2],
                             AVEncoderAudioQualityKey: [NSNumber numberWithInt: AVAudioQualityMin]};
  
  NSError *error;
  NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
  self.recorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
  [self.recorder prepareToRecord];
  [self.recorder setMeteringEnabled:YES];
  [self.recorder record];
  
  CADisplayLink *displaylink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateMeters)];
  [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

#pragma mark - Public Methods

- (void)updateSceneStyle:(GorillaDrummerSceneStyle)style {
  UIColor* gradientTopColor;
  UIColor* gradientBottomColor;
  UIImage* playButtonImage = nil;
  UIImage* stopButtonImage = nil;
  switch (style) {
    case GorillaDrummerSceneStyleBlue: {
      gradientTopColor = [UIColor colorWithRed:51.0f/255.0f green:186.0f/255.0f blue:221.0f/255.0f alpha:1.0f];
      gradientBottomColor = [UIColor colorWithRed:19.0f/255.0f green:36.0f/255.0f blue:116.0f/255.0f alpha:1.0];
      playButtonImage = [UIImage imageNamed:@"BtnPlayBlue"];
      stopButtonImage = [UIImage imageNamed:@"BtnStopBlue"];
    }
      break;
    case GorillaDrummerSceneStyleRed: {
      gradientTopColor = [UIColor colorWithRed:242.0f/255.0f green:1444.0f/255.0f blue:149.0f/255.0f alpha:1.0f];
      gradientBottomColor = [UIColor colorWithRed:166.0f/255.0f green:19.0f/255.0f blue:19.0f/255.0f alpha:1.0];
      playButtonImage = [UIImage imageNamed:@"BtnPlayRed"];
      stopButtonImage = [UIImage imageNamed:@"BtnStopRed"];
    }
      break;
    case GorillaDrummerSceneStyleGreen: {
      gradientTopColor = [UIColor colorWithRed:75.0f/255.0f green:207.0f/255.0f blue:143.0f/255.0f alpha:1.0f];
      gradientBottomColor = [UIColor colorWithRed:1.0f/255.0f green:108.0f/255.0f blue:96.0f/255.0f alpha:1.0];
      playButtonImage = [UIImage imageNamed:@"BtnPlayGreen"];
      stopButtonImage = [UIImage imageNamed:@"BtnStopGreen"];
    }
      break;
    case GorillaDrummerSceneStylePurple: {
      gradientTopColor = [UIColor colorWithRed:138.0f/255.0f green:46.0f/255.0f blue:226.0f/255.0f alpha:1.0f];
      gradientBottomColor = [UIColor colorWithRed:9.0f/255.0f green:3.0f/255.0f blue:14.0f/255.0f alpha:1.0];
      playButtonImage = [UIImage imageNamed:@"BtnPlayPurple"];
      stopButtonImage = [UIImage imageNamed:@"BtnStopPurple"];
    }
      break;
    case GorillaDrummerSceneStyleYellow: {
      gradientTopColor = [UIColor colorWithRed:255.0f/255.0f green:223.0f/255.0f blue:111.0f/255.0f alpha:1.0f];
      gradientBottomColor = [UIColor colorWithRed:222.0f/255.0f green:136.0f/255.0f blue:39.0f/255.0f alpha:1.0];
      playButtonImage = [UIImage imageNamed:@"BtnPlayYellow"];
      stopButtonImage = [UIImage imageNamed:@"BtnStopYellow"];
    }
      break;
  }
  [_gradientView updateGradientWithTopColor:gradientTopColor
                                bottomColor:gradientBottomColor];
  [_playButton setImage:playButtonImage forState:UIControlStateNormal];
  [_playButton setImage:stopButtonImage forState:UIControlStateSelected];
  [[self _gorillaDrummerScene] updateSceneStyle:style];
}

#pragma mark - Actions

- (IBAction)disappointedButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaDrummerExpressionAnimation:GorillaDrummerExpressionDisappointed];
}

- (IBAction)neutralButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaDrummerExpressionAnimation:GorillaDrummerExpressionNeutral];
}

- (IBAction)happyButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaDrummerExpressionAnimation:GorillaDrummerExpressionHappy];
}

- (IBAction)beatType0ButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaBeatAnimationForType:GorillaDrummerBeatType_0];
}

- (IBAction)beatType1ButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaBeatAnimationForType:GorillaDrummerBeatType_1];
}

- (IBAction)beatType2ButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaBeatAnimationForType:GorillaDrummerBeatType_2];
}

- (IBAction)beatType3ButtonAction:(id)sender {
  [[self _gorillaDrummerScene] showGorillaBeatAnimationForType:GorillaDrummerBeatType_3];
}

- (IBAction)backButtonAction:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playButtonAction:(id)sender {
  _playButton.selected = !_playButton.selected;
}

- (IBAction)coinsButtonAction:(id)sender {
  //TODO:
}

- (IBAction)coinsTextAnimation:(id)sender {
  [[self _gorillaDrummerScene] showCoinsTextAnimation:@"Oops. You can do better than this!"
                                        numberOfCoins:10
                                               awhile:YES];
}

- (void)updateMeters {
  [self.recorder updateMeters];
  CGFloat normalizedValue = [self _normalizedPowerLevelFromDecibels:[self.recorder averagePowerForChannel:0]];
  [[self _gorillaDrummerScene].wavesSpriteNode updateWithPeakLevel:normalizedValue];
}

- (CGFloat)_normalizedPowerLevelFromDecibels:(CGFloat)decibels {
  if (decibels < -60.0f || decibels == 0.0f) {
    return 0.0f;
  }
  
  return powf((powf(10.0f, 0.05f * decibels) - powf(10.0f, 0.05f * -60.0f)) * (1.0f / (1.0f - powf(10.0f, 0.05f * -60.0f))), 1.0f / 2.0f);
}

#pragma mark - Helpers

- (GorillaDrummerScene *)_gorillaDrummerScene {
  return (GorillaDrummerScene *)[(SKView *)self.gorillaSceneView scene];
}

@end

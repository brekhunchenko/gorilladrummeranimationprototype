//
//  MMSplashScreenViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/31/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMSplashScreenViewController.h"

@interface MMSplashScreenViewController ()

@end

@implementation MMSplashScreenViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

@end

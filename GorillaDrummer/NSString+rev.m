//
//  NSString+rev.m
//  RealEstateVids
//
//  Created by Alex on 3/27/15.
//  Copyright (c) 2015 itomych. All rights reserved.
//

#import "NSString+rev.h"
#import "NSURL+rev.h"

@implementation NSString (rev)

+ (NSString*)generateUniqueFileNameWithExtension:(NSString*)extension {
    
    NSString *guid = [[NSUUID new] UUIDString];

    if (extension.length) {
        return [guid stringByAppendingPathExtension:extension];
    }
    return guid;
}

+ (NSString*)uniqueFilePathsInDocumentsDirectoryWithName:(NSString*)name withExtension:(NSString*)extension{
    
    NSString *uniqueFileName = [self generateUniqueFileNameWithExtension:extension];
    
    NSString *resultString = [[NSString applicationDocumentsDirectory] stringByAppendingPathComponent:[name stringByAppendingString:uniqueFileName]];
    
    [NSURL addSkipBackupAttributeToItemAtPath:resultString];
    
    return resultString;
}

+ (NSString*)filePathInDocumentsDirectoryWithName:(NSString*)name withExtension:(NSString*)extension{
    
    NSString *uniqueFileName = [self generateUniqueFileNameWithExtension:extension];
    
    NSString *resultString = [[NSString applicationDocumentsDirectory] stringByAppendingPathComponent:[uniqueFileName stringByAppendingString:name]];
    
    return resultString;
}

+ (NSString*)uniqueFilePathsInLibraryCachesDirectoryWithName:(NSString*)name withExtension:(NSString*)extension {
    
    NSString *uniqueFileName = [self generateUniqueFileNameWithExtension:extension];
    
    NSString *resultString = [[NSString applicationDocumentsDirectory] stringByAppendingPathComponent:[name stringByAppendingString:uniqueFileName]];
    
    [NSURL addSkipBackupAttributeToItemAtPath:resultString];
    
    return resultString;
}

+ (NSString *)applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

+ (BOOL)validateEmailString:(NSString *)email {
    
    BOOL sticterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = sticterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

- (NSString *)re_stringByReplacingOccurrencesOfString:(NSString *)target withString:(NSString *)replacement {
    if (!replacement || [replacement isEqualToString:@"(null)"]) {
        replacement = @"";
    }
    return [self stringByReplacingOccurrencesOfString:target withString:replacement];
}

@end

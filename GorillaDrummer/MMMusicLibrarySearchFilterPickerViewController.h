//
//  MMMusicLibrarySearchFilterPickerView.h
//  GorillaDrummer
//
//  Created by Yaroslav on 11/1/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MMMusicLibrarySearchFilterPickerViewControllerDelegate;

@interface MMMusicLibrarySearchFilterPickerViewController : UIViewController

+ (MMMusicLibrarySearchFilterPickerViewController *)musicLibrarySearchFilterPickerViewController:(CGRect)frame
                                                               listOfStrings:(NSArray<NSString *> *)listOfStrings;

@property (nonatomic, strong) NSString* recentlySelectedFilterString;

@property (nonatomic, weak) id<MMMusicLibrarySearchFilterPickerViewControllerDelegate> delegate;

@end

@protocol MMMusicLibrarySearchFilterPickerViewControllerDelegate <NSObject>

- (void)musicLibrarySearchFilterPickerViewController:(MMMusicLibrarySearchFilterPickerViewController *)filterPickerView
                                       didSelectItem:(NSString *)filterString;

@end

@interface MMMusicLibrarySearchFilterCell : UITableViewCell

@end

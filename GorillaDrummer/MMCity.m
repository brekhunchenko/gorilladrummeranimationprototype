//
//  MMCity.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/28/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMCity.h"

@implementation MMCity

#pragma mark - Initializers

- (instancetype)initWithDictionary:(NSDictionary *)dict {
  self = [super init];
  if (self) {
    _countryName = [dict objectForKey:@"country"];
    _cityName = [dict objectForKey:@"name"];
  }
  return self;
}

@end

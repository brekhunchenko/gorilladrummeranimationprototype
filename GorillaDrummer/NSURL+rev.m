//
//  NSURL+rev.m
//  RealEstateVids
//
//  Created by Office Tomych on 4/23/15.
//  Copyright (c) 2015 itomych. All rights reserved.
//

#import "NSURL+rev.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "NSString+rev.h"

static NSString *kFamilyPremiereAppPrefix = @"app.familypremiere://";

@implementation NSURL (rev)

+ (NSURL *) applicationPhotoDirectory {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *possibleURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *appDocumentDirectory = nil;
    
    if ([possibleURLs count] >= 1) {
        appDocumentDirectory = [possibleURLs objectAtIndex:0];
    }
    return appDocumentDirectory;
}

+ (void) assetURLFromAbsoluteString:(NSString *)absoluteString withCompletion:(void (^)(NSURL *assetURL))completion {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  
    ALAssetsLibrary *library = [ALAssetsLibrary new];
    
    [library assetForURL:[NSURL URLWithString:absoluteString] resultBlock:^(ALAsset *asset) {
        
        if (asset) {
            completion(asset.defaultRepresentation.url);
        } else {
            NSURL *avAssetURL = [NSURL fileURLWithPath:absoluteString];
            AVAsset *asset = [AVAsset assetWithURL:avAssetURL];
            if (asset) {
                completion(avAssetURL);
            } else {
                completion(nil);
            }
        }
        
    } failureBlock:^(NSError *error) {
        
        NSURL *avAssetURL = [NSURL fileURLWithPath:absoluteString];
        AVAsset *asset = [AVAsset assetWithURL:avAssetURL];
        
        if (asset) {
            completion(avAssetURL);
        } else {
            completion(nil);
        }
        
    }];
#pragma GCC diagnostic pop
}

//+ (void) copyAlAsset:(ALAsset*)alasset withCompletion:(void (^)(NSURL *))completion {
//    dispatch_async(re_image_processing_queue(), ^{
//        
//        NSString *toPath = [NSString uniqueFilePathsInDocumentsDirectoryWithName:@"REV" withExtension:@"mov"];
//        NSURL *toURL     = [NSURL fileURLWithPath:toPath];
//        NSError *error = nil;
//        
//        [[NSFileManager defaultManager] createFileAtPath:toPath contents:nil attributes:nil];
//        
//        NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:toPath];
//        
//        static const NSUInteger BufferSize = 1024*1024;
//        
//        ALAssetRepresentation *rep = [alasset defaultRepresentation];
//        uint8_t *buffer = calloc(BufferSize, sizeof(*buffer));
//        NSUInteger offset = 0, bytesRead = 0;
//        
//        NSError *copyError = nil;
//        
//        do {
//            bytesRead = [rep getBytes:buffer fromOffset:offset length:BufferSize error:&copyError];
//            [handle writeData:[NSData dataWithBytesNoCopy:buffer length:bytesRead freeWhenDone:NO]];
//            offset += bytesRead;
//
//        } while (bytesRead > 0);
//        
//        free(buffer);
//        
//        [handle closeFile];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (error) {
//                completion(nil);
//            } else {
//                [NSURL addSkipBackupAttributeToItemAtURL:toURL];
//                completion(toURL);
//            }
//        });
//    });
//}

+ (BOOL) addSkipBackupAttributeToItemAtURL:(NSURL *)URL

{
    if ([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) {
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        
        return success;
    }
    return NO;
}

+ (BOOL) addSkipBackupAttributeToItemAtPath:(NSString *)filePath

{
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    return [self addSkipBackupAttributeToItemAtURL:fileURL];
}

- (BOOL)isInternalURL {
    NSString *path = [self absoluteString];
    
    return [path hasPrefix:kFamilyPremiereAppPrefix];
}

- (NSURL*)URLByRemovingInternalAppDomainName {
    
    NSString *path = [self absoluteString];
    
    path = [path stringByReplacingOccurrencesOfString:kFamilyPremiereAppPrefix withString:@""];
    
    return [NSURL URLWithString:path];
}

@end

//
//  MMCityPicker.m
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/28/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import "MMCityPicker.h"

@interface MMCityPicker ()  <UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation MMCityPicker

#pragma mark - Initializers

- (instancetype)init {
  self = [super init];
  if (self) {
    [self _setupProvinces];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  
  [self _setupProvinces];
}

#pragma mark - Setup

- (void)_setupProvinces {
  self.dataSource = self;
  self.delegate = self;
}

#pragma mark - Class Methods

- (void)setupCitiesForCountryName:(NSString *)countryName {
  _countryName = countryName;
  
  [self reloadAllComponents];
}

#pragma mark - Custom Setters & Getters

- (void)setCity:(MMCity *)city {
  __block NSInteger cityIdx = NSNotFound;
  [[self.class citiesForCountryName:_countryName] enumerateObjectsUsingBlock:^(MMCity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    if ([obj.cityName isEqualToString:city.cityName]) {
      cityIdx = idx;
      *stop = YES;
    }
  }];
  _city = city;
  if (cityIdx != NSNotFound) {
    [self selectRow:cityIdx inComponent:0 animated:NO];
  } else {
    NSLog(@"Error: city not found.");
  }
}

#pragma mark - UIPickerView

- (NSInteger)numberOfComponentsInPickerView:(__unused UIPickerView *)pickerView {
  return 1;
}

- (NSInteger)pickerView:(__unused UIPickerView *)pickerView numberOfRowsInComponent:(__unused NSInteger)component {
  return (NSInteger)[[self class] citiesForCountryName:self.countryName].count;
}

- (UIView *)pickerView:(__unused UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(__unused NSInteger)component reusingView:(UIView *)view {
  if (!view) {
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 3, 245, 24)];
    label.backgroundColor = [UIColor clearColor];
    label.tag = 1;
    [view addSubview:label];
    
    UIImageView *flagView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 24, 24)];
    flagView.contentMode = UIViewContentModeScaleAspectFit;
    flagView.tag = 2;
    [view addSubview:flagView];
  }
  
  ((UILabel *)[view viewWithTag:1]).text = [[[self class] citiesForCountryName:self.countryName][(NSUInteger)row] cityName];
  
  return view;
}

- (void)pickerView:(__unused UIPickerView *)pickerView
      didSelectRow:(__unused NSInteger)row
       inComponent:(__unused NSInteger)component {
  _city = [[self class] citiesForCountryName:self.countryName][(NSUInteger)row];
  if ([self.cityPickerDelegate respondsToSelector:@selector(cityPicker:didSelectCity:)]) {
    [self.cityPickerDelegate cityPicker:self didSelectCity:_city];
  }
}

#pragma mark - Class Methods

+ (NSArray<MMCity *> *)citiesForCountryName:(NSString *)countryName {
  static NSArray* provincesInfo = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"world_cities" ofType:@"json"];
    NSError * error = nil;
    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    NSData * jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    
    provincesInfo = parsedData;
  });
  
  NSMutableArray* provincesMutable = [NSMutableArray new];
  for (NSDictionary* dict in provincesInfo) {
    if ([[dict objectForKey:@"country"] isEqualToString:countryName]) {
      MMCity* city = [[MMCity alloc] initWithDictionary:dict];
      [provincesMutable addObject:city];
    }
  }
  
  return provincesMutable;
}

+ (MMCity *)cityForCityName:(NSString *)cityName countyName:(NSString *)countryName {
  __block NSInteger cityIdx = NSNotFound;
  [[self.class citiesForCountryName:countryName] enumerateObjectsUsingBlock:^(MMCity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    if ([obj.cityName isEqualToString:cityName]) {
      cityIdx = idx;
      *stop = YES;
    }
  }];
  
  if (cityIdx != NSNotFound) {
    return [[self citiesForCountryName:countryName] objectAtIndex:cityIdx];
  } else {
    return nil;
  }
}

@end

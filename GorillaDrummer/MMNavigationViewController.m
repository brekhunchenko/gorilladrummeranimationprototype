//
//  MMNavigationViewController.m
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMNavigationViewController.h"

@interface MMNavigationViewController ()

@end

@implementation MMNavigationViewController

#pragma mark - UIViewController 

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationBar.shadowImage = nil;
}

@end

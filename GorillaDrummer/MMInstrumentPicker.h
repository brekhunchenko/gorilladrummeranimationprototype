//
//  MMInstrumentsPicker.h
//  GorillaDrummer
//
//  Created by Yaroslav Brekhunchenko on 7/28/17.
//  Copyright © 2017 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMStgringPickerController.h"

@protocol MMInstrumentsPickerDelegate;

@interface MMInstrumentPicker : MMStgringPickerController

@property (nonatomic, weak) id<MMInstrumentsPickerDelegate> instrumentsPickerDelegate;
@property (nonatomic, strong) NSString* instrument;

+ (NSArray<NSString *> *)listOfMusicInstruments;

@end

@protocol MMInstrumentsPickerDelegate <NSObject>

- (void)instrumentsPicker:(MMInstrumentPicker *)instrumentsPicker didSelectInstrument:(NSString *)instrument;

@end

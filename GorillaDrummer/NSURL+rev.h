//
//  NSURL+rev.h
//  RealEstateVids
//
//  Created by Office Tomych on 4/23/15.
//  Copyright (c) 2015 itomych. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface NSURL (rev)

+ (NSURL *) applicationPhotoDirectory;

+ (void) assetURLFromAbsoluteString:(NSString *)absoluteString withCompletion:(void (^)(NSURL *assetURL))completion;

//+ (void) copyAlAsset:(ALAsset*)alasset withCompletion:(void (^)(NSURL *))completion;

+ (BOOL) addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

+ (BOOL) addSkipBackupAttributeToItemAtPath:(NSString *)filePath;

- (BOOL)isInternalURL;

- (NSURL*)URLByRemovingInternalAppDomainName;

@end

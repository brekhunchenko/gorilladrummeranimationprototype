//
//  MMNavigationBar.h
//  GorillaDrummer
//
//  Created by Yaroslav on 10/23/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kMMNavigationBarHeight = 82.0f;

@interface MMNavigationBar : UINavigationBar

@end
